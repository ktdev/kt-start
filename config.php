<?php

    /**
    * KT START - CONFIGURATION FILE
    * 
    * Author: ken@kentaro.be  - www.ktdev.info 
    * Under Licence MIT
    */
    
    // Répertoire racine de l'application
    if (! defined ( 'SP_ROOT' )) define( 'SP_ROOT', getcwd() );    
      
    // Séparateur
    if (! defined ( 'DS' )) define( 'DS', '/' );
    
    // Numéro de version de l'application
    if (! defined ( 'SP_VER')) define( 'SP_VER', '0.21.46 beta');
    
    // Date de mise à jour de l'application
    if (! defined ( 'SP_DATEMAJ')) define( 'SP_DATEMAJ', '25.06.2016 21:02');
    
    // Répertoire principal
    if (! defined ( 'SP_CORE')) define( 'SP_CORE', 'core');
    
    // Répertoire des datas
    if (! defined ( 'SP_DATAS')) define( 'SP_DATAS', 'datas');
    
    // Sous-répertoire des Classes
    if (! defined ( 'SP_CLASS')) define( 'SP_CLASS', 'class');
    
    // Sous-répertoire du Profil
    if (! defined ( 'SP_PROFILES')) define( 'SP_PROFILES', 'profiles');
    
    // Sous-répertoire des fichiers CSS
    if (! defined ( 'SP_CSS')) define( 'SP_CSS', 'css');
    
    // Sous-répertoire des fichiers Images
    if (! defined ( 'SP_IMG')) define( 'SP_IMG', 'img');
    
    // Sous-répertoire des fichiers JS
    if (! defined ( 'SP_JS')) define( 'SP_JS', 'js');
    
    // Sous-répertoire des librairies
    if (! defined ( 'SP_LIBS')) define( 'SP_LIBS', 'libs');
    
    // Sous-répertoire de stockage
    if (! defined ( 'SP_STORE')) define( 'SP_STORE', 'store');
    
    // Sous-répertoire des exports
    if (! defined ( 'SP_EXPORT')) define( 'SP_EXPORT', 'exports');
    
    // Sous-répertoire des imports
    if (! defined ( 'SP_IMPORT')) define( 'SP_IMPORT', 'imports');
    
    // Version Bootstrap
    if (! defined ( 'SP_BOOTSV')) define( 'SP_BOOTSV', '3.3.1');
    
    // Version Font-Awesome
    if (! defined ( 'SP_FONTV')) define( 'SP_FONTV', '4.3.0');
    
    // Chemin et nom du fichier de paramètres
    if (! defined ( 'SP_PARAMS')) define( 'SP_PARAMS', SP_DATAS.DS.SP_PROFILES.DS.'params.ini');    
    
        
?>
