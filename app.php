<?php

/**
* KT START - APP 
* 
* Author: ken@kentaro.be  - www.ktdev.info 
* Under Licence MIT
*/


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// ++++++++++++++++++ INITIALISATION VARIABLES +++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
$displayForm = NULL;
$msg = NULL;
$sendForm = NULL;
$addUrlForm = NULL;
$lastUrl = NULL;
$id = NULL;
$addMsg = NULL;
$msgUrlParam = NULL;
$msgTagsParam = NULL;
$flagFlash = FALSE;
$status = NULL;
$list = NULL;

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// +++++++++++++++++++++ SESSIONS TRAITEMENT +++++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

// Instanciation d'une nouvelle Session
$Session = new Session();

if(!isset($_SESSION['session'])) {
    // On charge les paramètres
    $Session->reloadParams();
    // Mise en session de l'objet Session 
    $_SESSION['session'] = $Session;
    // Sérialisation de la Session
    /*
    $Sess = serialize($Session);
    // Enregistre la session sérialisée
    if(!$Session->storeSession($Sess))
    $Session->setFlash('L\'enregistrement de la session a echoué, veuillez vérifier les droits d\'accès au répertoire:<strong>'.SP_CORE.DS.SP_SESS.'</strong>' ,'danger');    
    */
}else
    // Sinon on initialise la variable $Session(conteneur de l'objet session) 
    // avec la variable de de session 'session' qui contient l'objet session existant
    // Ceci évite le chargement des paramètres (accès fichiers) inutilement
    $Session = $_SESSION['session'];

// Affichage d'un message dans le cas où le fichier profil de l'utilisateur est inexistant
if($profileError)
    $Session->setFlash('!!! Attention !!! <br>Le fichier du profil utilisateur est inexistant. L\'identification est impossible !.
        Effacez le fichier params.ini situé à la racine et relancer l\'initialisation de KT Start.','danger'); 


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// +++++++++++++++++++++ DATAS TRAITEMENT +++++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

// Instanciation d'un nouvel objet Datas
$Datas = new Datas($Session);
$statusDatas = $Datas->getStatus();

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// +++++++++++++++++++ SECTION GET TRAITEMENT ++++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

// Vérifie si l'url retourne une vue
if( !empty($_GET['view'])) 
    $_SESSION['view'] = $_GET['view'];

// Vérifie si l'url retourne un filtre
if(!empty($_GET['filter'])) 
    $_SESSION['filter'] = $_GET['filter'];
elseif(!isset($_SESSION['filter']))
    $_SESSION['filter'] = 'aucun';

// Vérifie si l'url retourne une liste
if(!empty($_GET['list'])) 
    $_SESSION['list'] = $_GET['list'];
elseif(!isset($_SESSION['list']))
    $_SESSION['list'] = NULL;    


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// ++++++++++++++++++++ SECTION POST TRAITEMENT ++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

// ++++++++++++++ Si l'action est l'IDENTIFICATION +++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'identification')
{
    // Initialisation du Token et du password
    $passwordK = $_POST['passwordK'];
    $token = $_POST['tokenForm'];
    
    if(!empty($userProfile) && !empty($passwordProfile))
        $matching = $Session->matchPasswd($passwordK, $token, $userProfile, $passwordProfile);
       

    // Vérifie l'ouverture de session
    if($Session->sessionOpen()) 
    {   
        // Si sessionOpen renvoi TRUE on affiche les outils    
        $displayForm  = displayFormTools();

    }else{
        $Session->setFlash('Accès refusé','danger'); 
        $displayForm = displayFormIdentification();
    }   

}

// +++++++++ Affichage du Form en fonction de la Session +++++++++++ // 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

// Si il y a une session valide en cours on affiche le Badge Tools
if($Session->sessionOpen())
    $displayForm  = displayFormTools();
else
    $displayForm = displayFormIdentification();
/*
// Si on est pas en cours d'initialisation
if(!isset($_SESSION['tokenInstall'])) 
$displayForm = displayFormIdentification();
else
$displayForm  = displayFormTools();       
*/

// ++++++++++++++++++++ Si l'action est un TRI +++++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'tri')
{
    // Initialisation 
    $order = $_POST['direction'];
    $sort = $_POST['field']; 

    $_SESSION['sort'] = $sort;

    if( $order == 'SORT_ASC' )
        $_SESSION['order'] = SORT_ASC;
    else
        $_SESSION['order'] = SORT_DESC;    
}

// ++++++++++++++++++++ Si l'action est un Export ++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'export')
{
    $out = NULL;
    $filename = 'KTStart-'.SP_DATAS.'-'.date("dmy-His").'.tar.gz';
    $pathExport = SP_STORE.DS.SP_EXPORT.DS;
    $cmd ='tar czvf '.$pathExport.$filename.' '.SP_DATAS;
    exec($cmd);

    if(file_exists($pathExport.$filename)){
        header("Content-Type: application/force-download");
        header("Content-Transfer-Encoding: application/x-gzip"); 
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0, public");
        header("Expires: 0"); 
        ob_clean();
        flush();
        readfile($pathExport.$filename);
        exit;
    }else{ 
        $Session->setFlash('KT Start a rencontré un problème lors de l\'Export, c\'est peut-être dû à un problème de droits sur le répertoire <strong>'.
            SP_STORE. '</strong>. Dans le cas contraire veuillez en avertir le développeur.','danger'); 


    }

}

// ++++++++++++++++++++ Si l'action est un Import ++++++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'import')
{   
    // Si il y a erreur lors du POST
    if ($_FILES['importFile']['error']) {     
        switch ($_FILES['importFile']['error']){     
            case 1: // UPLOAD_ERR_INI_SIZE
                $Session->setFlash( 'Le fichier dépasse la limite autorisée par le serveur' ,'danger' );      
                break;     
            case 2: // UPLOAD_ERR_FORM_SIZE
                $Session->setFlash( 'Le fichier dépasse la limite autorisée dans le formulaire' ,'danger' );      
                break;     
            case 3: // UPLOAD_ERR_PARTIAL
                $Session->setFlash( 'L\'envoi du fichier a été interrompu pendant le transfert' ,'danger' );     
                break;     
            case 4: // UPLOAD_ERR_NO_FILE
                $Session->setFlash( 'Le fichier que vous avez envoyé a une taille nulle' ,'danger' );     
                break;     
        }     
    }     
    else {
        //Si il n'y a eu aucune erreur     
        if ($_FILES['importFile']['error'] == 0) { 

            // Initialisations
            $nomFileImport = $_FILES['importFile']['name'];
            $pathFileImport = pathinfo($nomFileImport);
            $extensionFileImport = $pathFileImport['extension'];
            $extensionsAutorisee = 'gz';
            $pathImport = SP_STORE.DS.SP_IMPORT.DS;

            // Si l'extension n'est pas correcte
            if ($extensionFileImport != $extensionsAutorisee) {
                $Session->setFlash( 'L\'extension du fichier d\'importation doit être: <strong>.tar.gz</strong>' ,'danger' );     

            } else { 

                $repertoireDestination = $pathImport;
                $filename = 'KTStart-'.SP_DATAS.'-'.date("dmy-His").'.tar.'.$extensionFileImport;

                // Copie du fichier dans le répertoire d'importation
                if (move_uploaded_file($_FILES["importFile"]["tmp_name"], $repertoireDestination.$filename)) {

                    $cmd ='rm -f '.SP_DATAS.DS.'*.* ';
                    exec($cmd);
                    $cmd ='cp '.SP_STORE.DS.SP_IMPORT.DS.$filename.' .'.DS;
                    exec($cmd);
                    $cmd ='tar -xzf '.$filename;
                    exec($cmd);
                    $cmd = 'rm -f '.$filename;
                    exec($cmd);
                    header('Location:index.php?view='.$pendingView); 
                    $Session->setFlash('Le fichier d\'importation à bien été déplacé vers <strong>'.$repertoireDestination.$filename.'</strong>', 'success');

                } else {
                    $Session->setFlash('Le système a rencontré un problème lors de la copie du fichier dans le répertoire d\'importation,
                        vérifiez les droits sur le répertoire: <strong>'.$repertoireDestination.'<strong>', 'danger');
                }
            }
        }     
    }     

}

// ++++++++++ Si l'action est l'édition des paramètres +++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'editParams')
{
    // Initialisation
    if( empty( $_POST['urlAppParam'] ) ) {
        $flagFlash = TRUE;
        $msgUrlParam = '<p>Veuillez entrer une valeur dans le champs "<strong>Url de l\'application</strong>"</p>';

    }else
        $url = $_POST['urlAppParam'];

    $view = $_POST['defaultViewParam'];
    $target = $_POST['targetParam']; 
    $badge = $_POST['bgThumbParam']; 
    $sort = $_POST['sortParam']; 
    $order = $_POST['orderParam'];

    if( empty( $_POST['tagsParam'] ) ) {
        $flagFlash = TRUE;
        $msgTagsParam = '<p>Veuillez entrer une valeur dans le champs "<strong>Tags par défaut</strong>"</p>';
    }else
        $tags = $_POST['tagsParam']; 

    $visibility = $_POST['visibilityParam'];
    //$list = $_POST['listParam'];   //TODO10
    $list = 'FAVORIS';

    if( $flagFlash === FALSE ) {


        $Session->setParams('URLAPP', $url);
        $Session->setParams('DEFAULT_VIEW', $view);
        $Session->setParams('TARGET', $target);
        $Session->setParams('DEFAULT_THUMB', $badge);
        $Session->setParams('DEFAULT_SORT', $sort);
        $Session->setParams('DEFAULT_ORDER', $order);
        $Session->setParams('DEFAULT_TAG', $tags);
        $Session->setParams('VISIBILITY', $visibility);
        $Session->setParams('DEFAULT_LIST', $list);

        $params['parameters'] = $Session->getParamsArray();

        $iniParamsEdit = new ini (SP_PARAMS);
        $iniParamsEdit->ajouter_array($params);
        $result = $iniParamsEdit->ecrire(TRUE);

        if($result['stat'] === FALSE)
            $Session->setFlash('La tentative de modification des paramètres a échouée','danger');
        else{ 
            $Session->reloadParams();
            $Session->setFlash('Les paramètres ont été modifiés et appliqués avec succès','success');        

        }


    }else
        $Session->setFlash( $msgUrlParam.$msgTagsParam ,'danger' ); 

}


// +++++++++++++ Si l'action est l'AJOUT d'un badge  +++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'addBadge')
{
    // Si on ajoute un badge  
    if(!empty ($_POST['urlBadge'])) {

        // On lance le processus uniquement si un Session est ouverte
        if($Session->sessionOpen()) {
            // +++++++++++ Traitements de l'url +++++++++++ //

            // Initialisation de la variable $urlBadge
            $urlBadge = $_POST['urlBadge'];

            // On parse l'url pour la décomposer
            $urlParsed = parse_url($urlBadge);

            // On vérifie l'existence du scheme dans l'url
            $keyResult = key_finder_urlparsed($urlParsed, 'scheme');

            // Si l'url ne possède pas de scheme (http:// ou https://) on l'ajoute
            if(!$keyResult) {

                // Ajouter le scheme manuellement en privilégiant le protocol https
                if(url_exist('https://'.$urlBadge)) {
                    $urlBadge = 'https://'.$urlBadge;
                }elseif(url_exist('http://'.$urlBadge))
                    $urlBadge = 'http://'.$urlBadge;
                else
                    $urlBadge = FALSE;    
            }

            // Si urlBadge contien FALSE on arrête le process et on affiche un message
            if(!$urlBadge)
                $Session->setFlash('Mauvaise url, veuillez vérifier et recommencer','danger');
            else{

                // On re-parse l'url pour la re-décomposer
                $urlReParsed = parse_url($urlBadge);

                // On vérifie si le host de l'url est une adresse IP
                $isIP = @checkIP($urlReParsed['host']);

                // Si ce n'est pas une IP
                if(!$isIP) {

                    $primaryDomain = getPrimaryDomain($urlBadge);

                    if ($primaryDomain != FALSE) {
                        $domain =  pathinfo ( $primaryDomain , PATHINFO_FILENAME );
                        $textThumb = $domain;
                    }else{
                        $textThumb = ''.substr($urlBadge, 0, 20);
                        $addMsg = '<br> Mais, il semblerait que TLD ne soit pas autorisé, veuillez l\'ajouter dans les paramètres';
                    }
                    // Si c'est une adresse IP
                }else {
                    $ipDomain = $urlReParsed['host'];
                    $textThumb = $ipDomain;
                    $primaryDomain = $ipDomain;
                } 

                $bgThumb = $_POST['bgThumb']; 

                // Si la variable de session LastUrl est définie et différente de NULL
                if(isset($_SESSION['lastUrl']))
                    $lastUrl = $_SESSION['lastUrl'];

                // Si l'url est identique à la précédente on affiche un message, sinon on lance le processus de creéation d'un nouveau Badge      
                if ($lastUrl == $urlBadge) { 
                    $Session->setFlash('Cette URL est identique à la précédente, elle ne sera pas ajoutée','danger');
                }else{

                    // On ajoute l'url à la variable de session de vérification de rafraichissement
                    $_SESSION['lastUrl'] = $urlBadge;

                    // Récupération des données de l'url entrée
                    $result = getUrlData($urlBadge);

                    // Date, heure et timestamp de création
                    $date = date('d/m/y H:i:s');
                    $timestamp = time();

                    // On lance la procédure de stockage du lien en créant un fichier
                    $datas = array();

                    // Génération du nom du fichier 
                    $filename = getToken().'.ini';

                    // +++ Création du fichier représentant le Badge +++ //

                    // Contrôles avant la création du fichier
                    // Si le background Thumb est vide alors prendre par défaut
                    if(empty($bgThumb))
                        $bgThumb = $_SESSION['defaultBgThumb'];

                    // Si le niveau de visibilité est non défini on prend le paramètre par défaut
                    if(empty($visibility))
                        $visibility = $_SESSION['visibility'];

                    // Si la liste est vide on envoi la liste de la session
                    if(empty($list))
                        $list = $_SESSION['list'];
                    // Si la liste d ela session pointe sur ALL, on prend le paramètre par défaut
                    if($_SESSION['list'] == 'ALL'){
                        $list = $Session->getParams('LIST');
                    }
                    
                    // Si le background Thumb est vide alors prendre par défaut
                    if(empty($tags))
                        $tags = $_SESSION['defaultTag'];

                    // Si le titre est vide alors initialiser sur: Titre non disponible  
                    if(empty($result['title']))
                        $titre = 'Titre non disponible';
                    else{
                        $titre = trim($result['title']);
                        $titre = html_entity_decode($titre);
                        $titre = htmlspecialchars($titre);
                    }


                    // Si la Description est vide alors noter: Description non disponible  
                    if(empty($result['metaTags']['description']['value']))
                        $description = 'Description non disponible';
                    else{
                        $description = frenchChar(trim($result['metaTags']['description']['value']));
                        $description = html_entity_decode($description);
                        $description = htmlspecialchars($description);
                    }

                    /*
                    if(empty($urlParsed['host']));
                    $urlReParsed['host'] = substr($urlBadge, 0, 20);
                    */

                    // On initialise un tableau avec les données
                    $datas['informations'] = array (
                        'visibility' => $visibility,
                        'list' => $list,
                        'url' => $urlBadge, 
                        'host' => $urlReParsed['host'],
                        'primaryDomain' => $primaryDomain,
                        'title' => $titre, 
                        'description' => $description,            
                        'textThumb' => ucfirst($textThumb),
                        'date' => $date,
                        'timestamp' => $timestamp, 
                        'bgThumb' => $bgThumb,
                        'tags' => $tags,
                        'filename' => $filename
                    );
                    // Instanciation la classe ini
                    $iniDatasAdd = new ini ('datas/'.$filename);
                    // Exécute la procédure d'ajout
                    $iniDatasAdd->ajouter_array($datas);
                    // Enregistre les données
                    $result = $iniDatasAdd->ecrire();

                    if($result['stat'] === FALSE) 
                        $Session->setFlash($result['msg'],'danger');
                    else {
                        $Datas->reloadDatas();
                        $Session->setFlash($result['msg'].$addMsg,'success');
                        //die();  
                        //
                        header('Refresh: 2; url=index.php'); 
                    }
                }
            } 

            // Si la session est expirée on affiche un message       
        }else {
            $Session->setFlash('Votre session est expirée, veuillez vous identifier','danger');    
        }
        // Si le formulaire de création d'un nouveau badge ne renvoi pas d'url    
    }else {
        $Session->setFlash('Veuillez entrer une URL','danger');   
    }  
   
}


// ++++++++++ Si l'action est la SUPPRESSION d'un badge ++++++++++++ // 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ // 
if(!empty($_POST['action']) && $_POST['action'] == 'delete')
{
    $filename = trim($_POST['id']);
    $return = unlink(SP_DATAS.DS.$filename);
    if($return)
    {
        $Datas->reloadDatas();
        $Session->setFlash('Le badge a été effacé','success'); 
        header('Refresh: 2; url=index.php');
    }else
        $Session->setFlash('le système a rencontré un problème lors de la suppression du fichier','danger');   

}

// +++++++++++++++ Si l'action est l'EDITION d'un badge ++++++++++++ // 
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ // 
if(!empty($_POST['action']) && $_POST['action'] == 'edition')
{

    $url =  trim($_POST['url']);
    $host =  trim($_POST['host']);
    $timestamp =  trim($_POST['timestamp']);
    $titre = trim($_POST['titre']);
    $description =  trim($_POST['description']);
    $visibility =  trim($_POST['visibility']);

    $textThumb =  trim($_POST['textThumb']);
    $textThumb = frenchChar($textThumb);

    $date =  trim($_POST['date']);
    if(empty($_POST['bgThumbEdit']))
        $bgThumb = $_SESSION['defaultBgThumb'];
    else
        $bgThumb =  trim($_POST['bgThumbEdit']);

    if(empty($_POST['ktlist']))
        $list = $_SESSION['list'];
    else    
        $list =  trim($_POST['ktlist']);

    // Si on crée une nouvelle liste
    if(!empty($_POST['newlist']))
        $list =  strtoupper(trim($_POST['newlist']));
    else{
       
        //Mise à jour de la liste avec liste déroulante 
        if(empty($_POST['ktlist']))
            $list = $_SESSION['list'];
        else    
            $list =  trim($_POST['ktlist']);    
    }    


    $tags =  trim($_POST['tags']);
    $id = trim($_POST['id']);

    $datas['informations'] = array (
        'visibility' => $visibility,
        'list' => $list,
        'url' => $url, 
        'host' => $host,
        'title' => $titre, 
        'description' => $description,            
        'textThumb' => $textThumb,
        'date' => $date, 
        'timestamp' => $timestamp,
        'bgThumb' => $bgThumb,
        'tags' => $tags,
        'filename' => $id       
    );


    $iniDatasEdit = new ini (SP_DATAS.DS.$id);
    $iniDatasEdit->ajouter_array($datas);
    $result = $iniDatasEdit->ecrire(TRUE);  
    
    if($result['stat'] === FALSE)
        $Session->setFlash('La tentative de modification du Badge a échouée','danger');
    else{ 
        $Datas->reloadDatas();
        $Session->setFlash('Le Badge a été modifié avec succès','success');
    }

}

// ++++++++++++++ Si l'action est une Initialisation +++++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
if(!empty($_POST['action']) && $_POST['action'] == 'initialize') {

    // Si tous les champs sont remplis
    if(!empty($_POST['usernameK']) && !empty($_POST['passwordK']) && !empty($_POST['passwordConfirmK']) && !empty($_POST['emailK']) && !empty($_POST['emailConfirmK'])
    && !empty($_POST['questionK']) && !empty($_POST['reponseK']) && !empty($_POST['tokenK'])) {


        if(isset($_SESSION['tokenInstall'])) {
            
            // Si les tokens correspondent
            if(!empty($_POST['tokenForm']) && $_SESSION['tokenInstall'] == crp::decrypte($_POST['tokenK'], $_SESSION['tokenInstall'])) 
            {

                /*
                $username = crp::decrypte($_POST['usernameK'], $_SESSION['tokenInstall']);
                $password = crp::decrypte($_POST['passwordK'], $_SESSION['tokenInstall']);
                $passwordConfirm = crp::decrypte($_POST['passwordConfirmK'], $_SESSION['tokenInstall']);
                $email = crp::decrypte($_POST['emailK'], $_SESSION['tokenInstall']);
                $emailConfirm = crp::decrypte($_POST['emailConfirmK'], $_SESSION['tokenInstall']);
                $question = crp::decrypte($_POST['questionK'], $_SESSION['tokenInstall']);
                $reponse = crp::decrypte($_POST['reponseK'], $_SESSION['tokenInstall']);
                $token = crp::decrypte($_POST['tokenK'], $_SESSION['tokenInstall']);
                //echo $password.'<br>'.$passwordConfirm.'<br>'.$email.'<br>'.$emailConfirm.'<br>'.$question.'<br>'.$reponse.'<br>'.$token.'<br>'.$_SESSION['tokenInstall'];
                */

                /* ++++ Génération du profil de l'utilisateur ++++ */
                /* +++++++++++++++++++++++++++++++++++++++++++++++ */
                $usernameK = $_POST['usernameK'];
                $passwordK = $_POST['passwordK'];
                $emailK = $_POST['emailK'];
                $questionK = $_POST['questionK'];
                $reponseK = $_POST['reponseK'];
                $tokenK = $_POST['tokenK'];

                $content = utf8_encode('<?php '
                    ."\n".'# User profile parameters'
                    ."\n".'$username = \''.$usernameK.'\';'
                    ."\n".'$passwordProfile = \''.$passwordK.'\';'
                    ."\n".'$emailProfile = \''.$emailK.'\';'
                    ."\n".'$userProfile = \''.$_SESSION['tokenInstall'].'\';'
                    ."\n".'$questionProfile = \''.$questionK.'\';'
                    ."\n".'$responseProfile = \''.$reponseK.'\';'
                    ."\n".'$tokenProfile = \''.$tokenK.'\';'                
                    ."\n".'?>');

                // Initialisation du nom de fichier du profil de l'utilisateur
                $profileFile = SP_DATAS.DS.SP_PROFILES.DS.'profile.php'; 

                // Création du fichier profile de l'utilisateur
                if ( !file_exists(SP_DATAS.DS.SP_PROFILES) ) {
                    echo'Le répertoire du profil utilisateur ('.SP_DATAS.DS.SP_PROFILES.') n\'existe pas.';
                    exit();
                    
                }else{
                    if (!$handle = fopen($profileFile, "wb+" )) {
                         echo 'L\'application ne peut ouvrir le fichier ('.$profileFile.')';
                         exit;
                    }else{
                        if (fwrite($handle, $content) === FALSE) {
                            echo 'L\'application ne peut écrire dans le fichier ('.$profileFile.')';
                            exit;
                        }else 
                            // On ferme l'objet fichier
                            fclose($handle);                            
                    }
                }
                
                /* ++++ Génération du fichier de paramètres ++++ */
                /* +++++++++++++++++++++++++++++++++++++++++++++ */

                // Génération de l'url d'installation
                $url = parse_url($_SERVER['HTTP_REFERER']);
                $scriptName = 'index.php';
                $autre = '';
                $url['path'] =preg_replace('`(^|\W)('.$scriptName.')(\W|$)`si','$1 '.$autre.' $3', $url['path']); 
                $installUrl = $url['scheme'].'://'.$url['host'].$url['path'];

                // Génération du tableau de paramètres par défaut
                $defaultParams = session::defaultParams($installUrl);
                $params['parameters'] = $defaultParams;

                // Tentative de création du fichier de paramètres
                    $createParams = new ini (SP_PARAMS);              
                    $createParams->ajouter_array($params);
                    $res = $createParams->ecrire(TRUE);
                    
                    if($res['stat'] === FALSE) {
                        echo $res['msg'];
                        exit;
                    }else{
                        
                        // Affichage du message de réussite de l'initialisation
                        $Session->setFlash('Félicitations! L\'initialisation de KT Start s\'est déroulée avec succès. Vous allez être redirigé d\'ici quelques instants' ,'success');
                        
                        // Destroy SESSION
                        session_destroy();
                        
                        // Rechargement de la page
                        header('Refresh: 5; url=index.php?view=BADGESVIEW');
                    }
               

            }else {
                $Session->setFlash('Système Erreur Token','danger');
            }
        }



    }else {
        $Session->setFlash('Tous les champs du formulaire n\'ont pas été rempli. Vous allez être redirigé dans quelques instants','danger');
        header('Refresh: 4; url=index.php');
    }
}
