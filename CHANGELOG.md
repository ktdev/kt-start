# Changelog

v 0.21.45
    Transfert du fichier profile de l'utilisateur dans le répertoire datas/profiles
v 0.20.45
    Correction d'un bug mineur et modifications diverses
v 0.20.44
    Optimisation du code : fonction getItemIniFile()

