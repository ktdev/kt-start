<?php

/**
* KT START - Logout
* 
* Author: kentaro@kentaro.be  - www.ktdev.info 
* Under Licence GPLV3
*/

// Initialise la session
session_start();

$pendingView = $_SESSION['view'];


// Unset toutes les variables des session
$_SESSION = array();

if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finally, destroy the session.
session_destroy();

// Renvoi vers la page index.php
header('Location:index.php?view='.$pendingView); 

?>

