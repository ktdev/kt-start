<?php
/**
* KT START - SERVER INFOS 
* 
* Author: kentaro@kentaro.be  - www.ktdev.info 
* Under Licence GPLV3
*/

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
// ++++++++++++++ LOADING CONFIGS, CLASSES & FONCTIONS +++++++++++++ //
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
require_once 'config.php';
require_once SP_CORE.DS.SP_CLASS.DS.'session.class.php';
require_once SP_CORE.DS.'functions.php';

$Session = new Session();
$Session = $_SESSION['session'];
?>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>KT Start | Informations sur le serveur</title>

        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-theme.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-select.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'font-awesome'.DS.SP_FONTV.DS.'css'.DS.'font-awesome.min.css'; ?>">

        <!-- Base Styles  -->
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.'css'.DS.'kt-start.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.'css'.DS.'server-infos.css'; ?>">

    </head>

    <body class="KTteam-background white-rabbit">
        <?php echo displayLightMenu(); ?>


        <div class="container-fluid">
            <div class="row">
                <?php echo $Session->flash();  $Session->unsetFlash(); ?>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    
                        <div class="row itemList">
                            <h1 class="text-center el_shadow">Informations sur le serveur</h1>
                            <div class="el_top20">
                            <?php
                            //$Sess = implode("", @file(SP_CORE.DS.SP_STORE.DS.'session-serialized'));
                            //$Session = unserialize($Sess);
                            if($Session->sessionOpen()) {
                                echo $Session->getInfosServer();
                            }else

                                echo'
                                <div class="col-md-3"></div>
                                <div id="alert" class="row text-center alert alert-dismissable alert-danger col-md-6" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Vous devez être identifié pour accéder à ces informations
                                </div> 
                                <div class="col-md-3"></div>
                                ';
                            ?>
                            </div><!-- .end el_top20-->
                        </div><!-- .end div row-->
                    
                </div><!-- .end div col-md-8-->

                <div class="col-md-2"></div>
            </div><!-- .end div row-->

            <?php 

            ?>
        </div>
        <!-- .end div container-->

        <!-- .end div container-->

        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'jquery-1.11.2.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'js'.DS.'bootstrap.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.'bootstrap-select'.DS.'bootstrap-select.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.'js'.DS.'kt-start.js' ?>"></script>   
    </body>
</html>