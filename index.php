<?php

    /**
    * KT START - INDEX 
    * 
    * Author: ken@kentaro.be  - www.ktdev.info 
    * Under Licence MIT
    */

    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
    // ++++++++++++++ LOADING CONFIGS, CLASSES & FONCTIONS +++++++++++++ //
    // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
    require_once 'config.php';
    require_once SP_CORE.DS.SP_CLASS.DS.'session.class.php';
    require_once SP_CORE.DS.SP_CLASS.DS.'datas.class.php';
    require_once SP_CORE.DS.SP_CLASS.DS.'ini.class.php';
    require_once SP_CORE.DS.SP_CLASS.DS.'crp.class.php';
    require_once SP_CORE.DS.'functions.php';

    // Chargement du profil de l'utilisateur
    if(file_exists(SP_DATAS.DS.SP_PROFILES.DS.'profile.php')) {
        $profileError = FALSE;
        require_once SP_DATAS.DS.SP_PROFILES.DS.'profile.php';	
    }else {
        $profileError = TRUE;
    }	


    if(checkFile(SP_PARAMS))
        require_once 'app.php';
    else {
        if(empty($_POST)) {
            // Instanciation d'une nouvelle Session
            $InitSess = new Session();
            $token = getToken();
            $_SESSION['tokenInstall'] = $token; 
            require_once 'initialize.php';
            die();
        }else {
            require_once 'app.php';    
        }

    }

?>

<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>KT Start | Your Personnal Homepage</title>

        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-theme.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-select.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'font-awesome'.DS.SP_FONTV.DS.'css'.DS.'font-awesome.min.css'; ?>">
        
        <!-- Favicons -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo SP_CORE.DS.SP_IMG.DS.'favicon.png'; ?>" />
        <link rel="icon" type="image/x-icon" href="<?php echo SP_CORE.DS.SP_IMG.DS.'favicon.png'; ?>" />

        <!-- Base Styles  -->
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.'css'.DS.'kt-start.css'; ?>">

    </head>

    <body class="KTteam-background white-rabbit">

        <?php echo displayMenu( $_SESSION['view'], $Session ); ?>

        <div class="container-fluid">
            <div class="starter-template">

                <div class="col-md-12">
                    <div class="container-fluid minHeight500">
                        <div class="row">
                            <?php echo $Session->flash();  $Session->unsetFlash(); ?>
                        </div>
                        <!-- .end div row-->

                        <?php 
                            // Affichage du form d'entrée (identification ou ajout d'une url)
                            echo $displayForm; 
                            
                            // Si il n'y a pas de datas 
                            if($statusDatas != NULL  ) {
                                //Si on est pas en cours d'initialisation -> Affiche un message
                                //if(!isset($_SESSION['tokenInstall']))
                                echo $Datas->displayMessageStatus($statusDatas); 
                            // Sinon le statut est NULL alors on affiche les données           
                            }else{
                                
                                switch($_SESSION['view'])
                                {
                                    case 'BADGESVIEW':  
                                        displayBadgesView($Datas, $_SESSION['sort'], $_SESSION['order'] , $Session, $_SESSION['filter'], $_SESSION['list']);
                                        break;
                                    case 'ARRAYVIEW' :
                                        displayArrayView($Datas, $_SESSION['sort'], $_SESSION['order'] , $Session, $_SESSION['filter'], $_SESSION['list']);
                                        break;
                                    case 'LISTVIEW' :
                                        displayListView($Datas, $_SESSION['sort'], $_SESSION['order'] , $Session, $_SESSION['filter'], $_SESSION['list']);
                                        break;
                                    case 'DEBUGVIEW' :
                                        displayDebugView($Datas, $_SESSION['sort'], $_SESSION['order'] , $Session, $_SESSION['filter'], $_SESSION['list']);
                                        break;
                                    default:
                                        displayBadgesView($Datas, $_SESSION['sort'], $_SESSION['order'] , $Session, $_SESSION['filter'], $_SESSION['list']);
                                }    
                            }
                        ?>

                    </div>
                    <!-- .end div container-fluid-->
                </div>
                <!-- .end div central-->


            </div>
            <!-- .end div starter-template-->
            <?php 
                echo displayModalDelete(); 
                echo displayModalEdit($Datas);
                echo displayModalSort();
                echo displayModalFilter($Datas);
                echo displayModalParams();
                echo displayModalInfos($Session, $Datas);
                echo displayModalTools();
            ?>
        </div>
        <!-- .end div container-->

        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'jquery-1.11.2.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.crp.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.md5.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.base64.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'js'.DS.'bootstrap.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.'bootstrap-select'.DS.'bootstrap-select.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.'js'.DS.'kt-start.js' ?>"></script>   
    </body>
</html>
