<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>KT Start | Initialisation</title>

        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-theme.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'css'.DS.'bootstrap-select.min.css'; ?>">
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.SP_LIBS.DS.'font-awesome'.DS.SP_FONTV.DS.'css'.DS.'font-awesome.min.css'; ?>">

        <!-- Base Styles  -->
        <link rel="stylesheet" href="<?php echo SP_CORE.DS.'css'.DS.'kt-start.css'; ?>">

    </head>

    <body class="KTteam-background white-rabbit">

        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="<?php echo SP_CORE.DS.SP_IMG.DS; ?>KT-Start2.png" alt="Logo KT-Start"></a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="container-fluid">
            <div class="starter-template">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h1 class="text-center KT_color KT_shadow">Initialisation de KT Start</h1>
                        <div class="el_top20">
                            <p class="el_grey initialize text-center"><span class="KT_color">KT Start</span> à besoin pour démarrer, d'<span class="flagSquare">un mot de passe</span> pour vous identifier 
                                ainsi que d'<span class="flagSquare">une adresse e-mail</span> et d'<span class="flagSquare">une réponse</span> à une question secrète. Ces derniers éléments seront utilisés uniquement en cas de modification de votre mot de passe.</p>
                            <form id="formInit" class="form-horizontal" action="index.php" role="form" method="POST">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="password" class="KTlabel col-xs-4 KT_color">Entrez un nom d'utilisateur <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="Identifiant" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="KTlabel col-xs-4 KT_color">Entrez votre mot de passe <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Mot de passe" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="passwordConfirm" class="KTlabel col-xs-4 KT_color">Confirmer votre mot de passe <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="password" class="form-control" id="passwordConfirm"  name="passwordConfirm" placeholder="Mot de passe confirmation" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="e-mail" class="KTlabel col-xs-4 KT_color">Entrez votre e-mail <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" id="email" name="email" placeholder="E-mail" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="e-mailConfirm" class="KTlabel col-xs-4 KT_color">Confirmer votre e-mail <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" id="emailConfirm" name="emailConfirm" placeholder="E-mail confirmation" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="question" class="KTlabel col-xs-4 KT_color">Question secrète <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <select class="form-control" id="question" name="question" required>
                                                <option value="0">Quel est le nom de jeune fille de votre mère</option>
                                                <option value="1">Quel est votre lieu de naissance</option>
                                                <option value="2">Quel est la couleur de vos yeux</option>
                                                <option value="3">Combien mesurez-vous (hauteur)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reponse" class="KTlabel col-xs-4 KT_color">Réponse à la question <span class="KT_color_2">*</span></label>
                                        <div class="col-xs-4">
                                            <input type="text" class="form-control" id="reponse" name="reponse" placeholder="Réponse à la question" required>
                                            <div class="error-message KT_red"></div>
                                        </div>
                                    </div>
                                    <div class="KT_color_2">* Tous les champs sont obligatoires</div>
                                    <div class="form-group text-center el_top50">
                                        <input type="hidden" id="action" name="action" value="initialize">
                                        <input type="hidden" id="tokenForm" name="tokenForm" value="<?php echo $token; ?>">
                                        
                                        <input type="hidden" id="usernameK" name="usernameK">
                                        <input type="hidden" id="passwordK" name="passwordK">
                                        <input type="hidden" id="passwordConfirmK" name="passwordConfirmK">
                                        <input type="hidden" id="emailK" name="emailK">
                                        <input type="hidden" id="emailConfirmK" name="emailConfirmK">
                                        <input type="hidden" id="questionK" name="questionK">
                                        <input type="hidden" id="reponseK" name="reponseK">
                                        <input type="hidden" id="tokenK" name="tokenK">
                                        
                                        <button type="submit" class="btn btn-default">Lancer l'initialisation KT Start</button>
                                    </div>
                                </div><!-- .end row -->
                            </form>
                        </div><!-- .end el_top20 -->

                    </div><!-- .end col-md-8 -->
                    <div class="col-md-2"></div>
                </div><!-- .end div row -->

            </div><!-- .end div starter-template -->

        </div><!-- .end div container-fluid -->

        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'jquery-1.11.2.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.crp.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.md5.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'crp'.DS.'jquery.base64.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'jquery.validate'.DS.'jquery.validate.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'jquery'.DS.'jquery.validate'.DS.'messages_fr.min.js'; ?>"></script>
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.SP_BOOTSV.DS.'js'.DS.'bootstrap.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.SP_LIBS.DS.'bootstrap'.DS.'bootstrap-select'.DS.'bootstrap-select.min.js'; ?>"></script> 
        <script src="<?php echo SP_CORE.DS.'js'.DS.'kt-start.js' ?>"></script>
        <script src="<?php echo SP_CORE.DS.'js'.DS.'kt-start-init.js' ?>"></script>   
    </body>
</html>
