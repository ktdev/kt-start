# KT-Start developed by KTDev#
KT Start est un applicatif développé pour gérer vos favoris et éventuellemennt remplacer la page de démarrage/d'accueil de votre navigateur.

KT-Start vous offre la possibilité de conserver et de gérer vos liens favoris et ce indépendamment de votre système d'exploitation et de votre navigateur.

KT-Start est une application web qui ne nécessite aucune base de données. Pour l'installer, il suffit de copier l'archive sur votre serveur web dans l'emplacement de votre choix, de désarchiver cette dernière et de vous rendre sur l'url pour complèter le mini formulaire de pré-installation.
