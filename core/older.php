<?php

/**
* Fonction qui affiche les informations dans 
*  sous la forme d'un Badge
*  
* @param string $url : L'url du site
* @param string $host :  Le Host du site
* @param string $title :  Le titre du site
* @param string $description : La description du site
* @param string $bgThumb : Le background du Badge
* @param string $textThumb :  Le texte affiché sur le Badge
* @param string $date : La date de création du badge
* @param string $tags : Les étiquettes du Badge
* @param string $filename : Le nom du fichier (id)
* @param string $Session : L'Objet Session
* @return string : Retourne le Badge au format Html
*/
function displayBadgeKT($url, $host, $title, $description, $bgThumb, $textThumb, $date, $tags, $visibility, $filename, $primaryDomain, $Session)
{
    // Ne garder que les 25 premiers charactères pour l'affichage du Host du Macaron et 20 caraxctères pour le textThumb
    $macaronUrl = $url;
    $macaronHost = substr($host, 0, 20);
    //$textThumb = substr($textThumb, 0, 20);


    $stringAdmin='
    <div class="macaron">
    <a class="text-center" href="'.$url.'" target="'.$_SESSION['target'].'">
    <div class="macaronThumb '.$bgThumb.'"><p class="textThumb">'.$textThumb.'</p></div>
    </a>
    <div class="macaronAdmin text-center">
    <a rel="'.$macaronHost.'" class="ancrePopover"><div class="macaronInfo popoverMacaron" data-placement="right" data-original-title="'.$host.'" data-content="<strong>Description</strong>: '.$description.'<hr><strong>Titre</strong>: '.$title.'<br><strong>Créé le</strong>: '.$date.'<br><strong>Url</strong>: '.$url.'<br><strong>Tags</strong>: '.$tags.'<br><strong>Visibilité</strong>: '.$visibility.'<hr><strong>Type de badge</strong>: '.$bgThumb.'<br><strong>Fichier</strong>: '.$filename.'"><i class="fa fa-info fa-lg"></i></div></a>
    <a href="#" data-id="'.$filename.'+S:E:P+'.$url.'+S:E:P+'.$title.'+S:E:P+'.$description.'+S:E:P+'.$bgThumb.'+S:E:P+'.$textThumb.'+S:E:P+'.$tags.'+S:E:P+'.$host.'+S:E:P+'.$primaryDomain.'+S:E:P+'.$date.'+S:E:P+'.$visibility.'" class="editBadge"><div class="macaronEdit" data-toggle="tooltip" data-placement="right" title="Edition du badge"><i class="fa fa-pencil fa-lg"></i></div></a>
    <a href="#" data-id="'.$filename.'+S:E:P+'.$url.'" class="deleteBadge"><div class="macaronDelete" data-toggle="tooltip" data-placement="right" title="Suppression du badge"><i class="fa fa-times fa-lg"></i></div></a>
    </div>
    <div class="macaronUrl">
    <a href="'.$url.'" target="'.$_SESSION['target'].'">'.$macaronHost.'</a>
    </div>
    </div>
    ';


    $stringConsult='
    <div class="macaron">
    <a class="text-center" href="'.$url.'" target="'.$_SESSION['target'].'">
    <div class="macaronThumb '.$bgThumb.'"><p class="textThumb">'.$textThumb.'</p></div>
    </a>
    <div class="macaronAdmin text-center">
    <a rel="'.$macaronHost.'" class="ancrePopover"><div class="macaronInfo popoverMacaron" data-placement="right" data-original-title="'.$host.'" data-content="<strong>Description</strong>: '.$description.'<hr><strong>Titre</strong>: '.$title.'<br><strong>Créé le</strong>: '.$date.'<br><strong>Url</strong>: '.$url.'<br><strong>Tags</strong>: '.$tags.'"><i class="fa fa-info fa-lg"></i></div></a>
    </div>
    <div class="macaronUrl">
    <a href="'.$url.'" target="'.$_SESSION['target'].'">'.$macaronHost.'</a>
    </div>
    </div>
    ';

    if($Session->sessionOpen())
        return $stringAdmin;
    else
        return $stringConsult;


}

/**
* Affiche le module principal de la StartPage
* Affiche les liens sous la forme d'un tableau
* 
* 
*/
function displayStartPageArray($directory, $Session)
{
    $nbThumb = 1;
    $path = SP_CORE.DS.SP_IMG.DS.'dots'.DS; 

    if( $dossier = opendir($directory.DS) )
    {

        $nbFiles = count(scandir($directory));

        if($nbFiles <= 2) {
            echo '<div class="row text-center"><div class="col-md-3"></div><div id="emptyDir" class="col-md-6">C\'est un peu vide ici...<br>Il est temps d\'ajouter un nouveau Badge !</div><div class="col-md-3"></div></div>';
            $_SESSION['lastUrl'] = NULL;
        }else { 

            echo '<div  class="table-responsive el_top20">';
            echo '<table class="table table-striped table-hover table-condensed table-primary">';

            $theadConsult = '<tr class="thTitle">
            <th class="text-center">N°</th>
            <th class="text-center hidden-xs">Badge</th>
            <th class="text-center">Site</th>
            <th class="text-center hidden-xs">Titre</th>
            <th class="text-center hidden-xs">Description</th>
            <th class="text-center hidden-xs">Tags</th>
            </tr>';
            $theadAdmin = '<tr class="thTitle">
            <th class="text-center">N°</th>
            <th class="text-center hidden-xs">Badge</th>
            <th class="text-center">Site</th>
            <th class="text-center hidden-xs">Titre</th>
            <th class="text-center hidden-xs">Description</th>
            <th class="text-center hidden-xs">Tags</th>
            <th class="text-center"><i class="fa fa-trash-o"></i></th>
            <th class="text-center"><i class="fa fa-pencil fa-lg"></i></th>
            </tr>';

            if($Session->sessionOpen())
                echo $theadAdmin;
            else
                echo $theadConsult;

            while( false !== ($fichier = readdir( $dossier )) )
            {

                $info = new SplFileInfo($fichier);
                $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                if( $fichier != '.' && $fichier != '..' && $extension == 'ini')
                {

                    $urlRead = trim(getItemIniFile( 'datas/'.$fichier, 'url', 'informations' ));
                    $hostRead = trim(getItemIniFile( 'datas/'.$fichier, 'host', 'informations' ));
                    $primaryDomainRead = trim(getItemIniFile( 'datas/'.$fichier, 'primaryDomain', 'informations' ));
                    $bgThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'bgThumb', 'informations' ));
                    $textThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'textThumb', 'informations' ));
                    $tagsRead = trim(getItemIniFile( 'datas/'.$fichier, 'tags', 'informations' ));
                    $descriptionRead = trim(getItemIniFile( 'datas/'.$fichier, 'description', 'informations' ));
                    $titleRead = trim(getItemIniFile( 'datas/'.$fichier, 'title', 'informations' ));
                    $dateRead = trim(getItemIniFile( 'datas/'.$fichier, 'date', 'informations' ));
                    $filenameRead = trim(getItemIniFile( 'datas/'.$fichier, 'filename', 'informations' ));
                    $visibilityRead = trim(getItemIniFile( 'datas/'.$fichier, 'visibility', 'informations' ));

                    // Shortified string fields
                    $shortUrlRead = substr($urlRead, 0, 50);
                    $descriptionReadShort = substr($descriptionRead, 0, 75);
                    $titleReadShort = substr($titleRead, 0, 40);


                    $tbodyConsult =  '<tr class="tdBody">
                    <td class="verticalAllign text-center">'.$nbThumb.'</td>
                    <td class="verticalAllign text-center hidden-xs"><img src="'.$path.'dot-'.$bgThumbRead.'.png"></td>
                    <td class="magnify"><a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a><br><small>'.$urlRead.'</small></td>
                    <td class="verticalAllign hidden-xs">'.$titleRead.'</td>
                    <td class="verticalAllign hidden-xs">'.$descriptionRead.'</td>
                    <td class="verticalAllign text-center hidden-xs">'.$tagsRead.'</td>
                    </tr>';

                    $tbodyAdmin =  '<tr class="tdBody">
                    <td class="verticalAllign text-center">'.$nbThumb.'</td>
                    <td class="verticalAllign text-center hidden-xs"><img src="'.$path.'dot-'.$bgThumbRead.'.png"></td>
                    <td class="magnify"><a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a><br><small>'.$urlRead.'</small></td>
                    <td class="verticalAllign hidden-xs">'.$titleReadShort.'</td>
                    <td class="verticalAllign hidden-xs">'.$descriptionReadShort.'</td>
                    <td class="verticalAllign text-center hidden-xs">'.$tagsRead.'</td>
                    <td class="verticalAllign text-center"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'" class="deleteBadge"><button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></a></td>
                    <td class="verticalAllign text-center"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'+S:E:P+'.$titleRead.'+S:E:P+'.$descriptionRead.'+S:E:P+'.$bgThumbRead.'+S:E:P+'.$textThumbRead.'+S:E:P+'.$tagsRead.'+S:E:P+'.$hostRead.'+S:E:P+'.$primaryDomainRead.'+S:E:P+'.$dateRead.'+S:E:P+'.$visibilityRead.'" class="editBadge"><button type="submit" class="btn btn-success"><i class="fa fa-pencil"></i></button></a></td>
                    </tr>';

                    if($Session->sessionOpen()){
                        echo $tbodyAdmin;
                        $nbThumb++;
                    }else {
                        if($Session->sessionOpen() === FALSE && $visibilityRead == 'public') {
                            echo $tbodyConsult;                                                                                                                                                                                                                                                                           
                            $nbThumb++;
                        }
                    }




                }
            }
            echo '</table>';
            echo '</div>';

            // Fermeture du dossier    
            closedir( $dossier );
        }
    }else {

        $st['stat'] = FALSE;
        $st['msg'] = 'Le dossier cible n\' a pas pu être ouvert';
        return $st;

    }
}

/**
* Affiche le module principal de la StartPage
* Affiche les liens sous la forme d'une suite de Badges
* inclus dans une div
* 
* @param string $directory
*/

function displayStartPageBadges($directory, $Session)
{
    $nbFiles = NULL;
    $nbThumb = 0; 

    if( $dossier = opendir($directory.DS) )
    {
        $nbFiles = count(scandir($directory));

        if($nbFiles <= 2) {
            echo '<div class="row text-center"><div class="col-md-3"></div><div id="emptyDir" class="col-md-6">C\'est un peu vide ici...<br>Il est temps d\'ajouter un nouveau Badge !</div><div class="col-md-3"></div></div>';
            $_SESSION['lastUrl'] = NULL;
        }else { 

            echo '<div class="row">';           

            while( false !== ($fichier = readdir( $dossier )) )
            {

                $info = new SplFileInfo($fichier);
                $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                if( $fichier != '.' && $fichier != '..' && $extension == 'ini')
                {

                    $urlRead = trim(getItemIniFile( 'datas/'.$fichier, 'url', 'informations' ));
                    $hostRead = trim(getItemIniFile( 'datas/'.$fichier, 'host', 'informations' ));
                    $primaryDomainRead = trim(getItemIniFile( 'datas/'.$fichier, 'primaryDomain', 'informations' ));
                    $bgThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'bgThumb', 'informations' ));
                    $textThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'textThumb', 'informations' ));
                    $descriptionRead = trim(getItemIniFile( 'datas/'.$fichier, 'description', 'informations' ));
                    $titleRead = trim(getItemIniFile( 'datas/'.$fichier, 'title', 'informations' ));
                    $dateRead = trim(getItemIniFile( 'datas/'.$fichier, 'date', 'informations' ));
                    $tagsRead = trim(getItemIniFile( 'datas/'.$fichier, 'tags', 'informations' ));
                    $filenameRead = trim(getItemIniFile( 'datas/'.$fichier, 'filename', 'informations' ));
                    $visibilityRead = trim(getItemIniFile( 'datas/'.$fichier, 'visibility', 'informations' ));

                    // Affiche les liens privés uniquement si l'on est identifié
                    if($Session->sessionOpen()) {
                        echo displayBadgeKT( $urlRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $primaryDomainRead, $Session);
                        $nbThumb++;

                    }elseif($Session->sessionOpen() === FALSE && $visibilityRead == 'public') {  
                        echo displayBadgeKT( $urlRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $primaryDomainRead, $Session);
                        $nbThumb++;                   
                    }

                }

            }

            echo '</div>';

            // Fermeture du dossier    
            closedir( $dossier );
        }
    }else {

        $st['stat'] = FALSE;
        $st['msg'] = 'Le dossier cible n\' a pas pu être ouvert';
        return $st;

    }
}

/**
* Affiche le module principal de la StartPage
* Affiche les liens sous la forme d'une Liste
* 
* 
*/
function displayStartPageList($directory, $Session)
{
    $nbThumb = 1;
    $path = SP_CORE.DS.SP_IMG.DS.'dots'.DS; 

    if( $dossier = opendir($directory.DS) )
    {

        $nbFiles = count(scandir($directory));

        if($nbFiles <= 2) {
            echo '<div class="row text-center"><div class="col-md-3"></div><div id="emptyDir" class="col-md-6">C\'est un peu vide ici...<br>Il est temps d\'ajouter un nouveau Badge !</div><div class="col-md-3"></div></div>';
            $_SESSION['lastUrl'] = NULL;
        }else { 

            echo '<div  class="col el_top20">';

            while( false !== ($fichier = readdir( $dossier )) )
            {

                $info = new SplFileInfo($fichier);
                $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                if( $fichier != '.' && $fichier != '..' && $extension == 'ini')
                {

                    $urlRead = trim(getItemIniFile( 'datas/'.$fichier, 'url', 'informations' ));
                    $hostRead = trim(getItemIniFile( 'datas/'.$fichier, 'host', 'informations' ));
                    $primaryDomainRead = trim(getItemIniFile( 'datas/'.$fichier, 'primaryDomain', 'informations' ));
                    $bgThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'bgThumb', 'informations' ));
                    $textThumbRead = trim(getItemIniFile( 'datas/'.$fichier, 'textThumb', 'informations' ));
                    $descriptionRead = trim(getItemIniFile( 'datas/'.$fichier, 'description', 'informations' ));
                    $titleRead = trim(getItemIniFile( 'datas/'.$fichier, 'title', 'informations' ));
                    $dateRead = trim(getItemIniFile( 'datas/'.$fichier, 'date', 'informations' ));
                    $tagsRead = trim(getItemIniFile( 'datas/'.$fichier, 'tags', 'informations' ));
                    $filenameRead = trim(getItemIniFile( 'datas/'.$fichier, 'filename', 'informations' ));
                    $visibilityRead = trim(getItemIniFile( 'datas/'.$fichier, 'visibility', 'informations' ));

                    // Icône de visibilité en fontion de l'état
                    if($visibilityRead == 'public')
                        $iconVis = '<i class="fa fa-unlock-alt"></i>';
                    else
                        $iconVis = '<i class="fa fa-lock"></i>';

                    // Mettre en évidence un nouveau badge ajouté en modifiant la classe du tag 
                    if($tagsRead == 'new')
                        $classTags = 'btnRed';
                    else
                        $classTags = 'btnGreen';

                    // Affiche les liens privés uniquement si l'on est identifié
                    if($Session->sessionOpen()) {
                        echo $item = '
                        <p class="itemList">
                        <span class="KTbtn hidden-xs">'.$nbThumb.'</span>
                        <span class="KTbtn item'.$visibilityRead.' hidden-xs" data-toggle="tooltip" data-placement="right" title="'.$visibilityRead.'">'.$iconVis.'</span>
                        <span class="KTbtn itemDelete hidden-xs" data-toggle="tooltip" data-placement="right" title="Suppression"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'" class="deleteBadge"><i class="fa fa-trash-o"></i></a></span>
                        <span class="KTbtn itemEdit hidden-xs" data-toggle="tooltip" data-placement="down" title="Edition"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'+S:E:P+'.$titleRead.'+S:E:P+'.$descriptionRead.'+S:E:P+'.$bgThumbRead.'+S:E:P+'.$textThumbRead.'+S:E:P+'.$tagsRead.'+S:E:P+'.$hostRead.'+S:E:P+'.$primaryDomainRead.'+S:E:P+'.$dateRead.'+S:E:P+'.$visibilityRead.'" class="editBadge"><i class="fa fa-pencil"></i></a></span>&nbsp;
                        <a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a>&nbsp;
                        <span class="btn KTbtn '.$classTags.' hidden-xs">'.$tagsRead.'</span>
                        </p>
                        ';
                        $nbThumb++;

                    }elseif($Session->sessionOpen() === FALSE && $visibilityRead == 'public') {  
                        echo $item = '
                        <p class="itemList">
                        <span class="KTbtn hidden-xs">'.$nbThumb.'</span>
                        <a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a>
                        <span class="btn KTbtn '.$classTags.' hidden-xs">'.$tagsRead.'</span>
                        </p>
                        ';                    
                        $nbThumb++;                   
                    }

                }
            }

            echo '</div>';

            // Fermeture du dossier    
            closedir( $dossier );
        }
    }else {

        $st['stat'] = FALSE;
        $st['msg'] = 'Le dossier cible n\' a pas pu être ouvert';
        return $st;

    }
}

