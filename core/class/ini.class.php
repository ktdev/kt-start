<?php

/**
* KT START - Class Ini
* Gestion et manupulation des fichiers ini
* 
* Author: http://www.sroze.io/2008/05/18/ecrire-des-fichiers-ini-simplement-avec-php/
* Adaptation: ken@kentaro.be  - www.ktdev.info 
* Under Licence MIT
*/


class ini
{

    var $ini;
    var $filename;

    public function __construct ($filename, $commentaire = FALSE) {
        $this->filename = $filename;
        $this->ini = (!$commentaire) ? '' : ';'.$commentaire;
    }

    public function ajouter_array ($array) {
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $this->sous_tableau($val, $key);
            }
            else if (is_string($key)) {
                $this->ajouter_valeur($key, $val);
            }
        }
    }

    private function sous_tableau ($tab, $groupe = FALSE) {
        if ($groupe) {
            $this->ini .='['.$groupe.']';
        }
        foreach ($tab as $key => $val) {
            if (!$this->ajouter_valeur($key, $val)) return FALSE;
        }
        $this->ini .= "\n";
        return TRUE;
    }

    private function ajouter_valeur ($key, $val) {
        if (is_array($val)) {

            $st['stat'] = FALSE;
            $st['msg'] = '<strong>Erreur :</strong> Impossible d\'ajouter une valeur';
            return $st; 

        }
        else if (is_string($val) OR is_double($val) OR is_int($val)) {
            $this->ini .= "\n".$key.'='.$val.'';
        }
        else {

            $st['stat'] = FALSE;
            $st['msg'] = '<strong>Erreur :</strong> Le type de donnée n\'est pas supporté';
            return $st; 
        }
        return TRUE;
    }

    public function ecrire ($rewrite = FALSE)
    {
        
        $c = TRUE;
        if(file_exists($this->filename)) {
            
            if($rewrite) {
                unlink($this->filename);
            }elseif(!$rewrite) {

                $st['stat'] = FALSE;
                $st['msg'] = '<strong>Erreur fatale :</strong> Le fichier ini existe déjà';
                $c = FALSE;
                return $st; 

            }
        }
        
        if ($c === TRUE)
        {
            
                $fichier = fopen($this->filename, 'w+');
                
                if (!$fichier) {
                    $st['stat'] = FALSE;
                    $st['msg'] = '<strong>Erreur fatale :</strong> Impossible d\'ouvrir le fichier';
                    return $st;
                }
                if (!fwrite($fichier, $this->ini)) {

                    $st['stat'] = FALSE;
                    $st['msg'] = '<strong>Erreur fatale :</strong> Impossible d\'écrire dans le fichier';
                    return $st;
                }

                $st['stat'] = TRUE;
                $st['msg'] = '<strong>Succès :</strong> Un nouveau Badge a été créé';
                return $st; 

                fclose($fichier);               
            
        }

    }
    
    public function updateIni($array) {
        
        $fichier = fopen($this->filename, 'w');
        ftruncate($fichier, 0);
        $this->ajouter_array($array);
        fclose($fichier);
           
    }
}
?>