<?php

/**
* KT START - Class Datas
* Gestion et manipulations des données
* 
* Author: ken@kentaro.be  - www.ktdev.info 
* Under Licence MIT
*/

class Datas {

    var $datasArray = array();
    var $datasDirectory = NULL;
    var $sortable_array = NULL;
    var $tags_array = NULL; 
    var $lists_array = NULL;   
    var $Session = NULL;
    var $status = NULL;


    public function __construct($Session) {

        //Initialisations 
        $this->datasDirectory = SP_DATAS.DS;
        $this->Session = $Session;

        // Chargement des données
        $this->loadDatas($this->datasDirectory);


    }



    /**
    * Méthode de chargement des données
    * Crée un tableau de données en récupérant les données des fichiers ini
    * du répertoire Datas de l'application
    * 
    * @param string $datasDirectory: Chemin du répertoire Datas
    */
    private function loadDatas($datasDirectory) {

        $index = 0;

        // Si il est possible d'ouvrir le répertoire
        if( $dossier = @opendir($datasDirectory) ) {
            // Compte le nombre de fichiers
            $nbFiles = count(scandir($datasDirectory));

            // Si seules les entrées '.' et '..' (2 fichiers) sont présentes -> le répertoire est vide, on affiche un message
            if($nbFiles <= 2) {

                // Le répertoire Datas est vide
                $this->status = 2;

            }else { 
                //$this->status = 1;
                while( false !== ($fichier = readdir( $dossier )) )
                {

                    $info = new SplFileInfo($fichier);
                    $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);

                    if( $fichier != '.' && $fichier != '..' && $extension == 'ini')
                    {

                        $this->datasArray[$index]['visibility'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'visibility', 'informations' ));
                        $this->datasArray[$index]['list'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'list', 'informations' ));
                        $this->datasArray[$index]['url'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'url', 'informations' ));
                        $this->datasArray[$index]['host'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'host', 'informations' ));
                        $this->datasArray[$index]['bgThumb'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'bgThumb', 'informations' ));
                        $this->datasArray[$index]['textThumb'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'textThumb', 'informations' ));
                        $this->datasArray[$index]['description'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'description', 'informations' ));
                        $this->datasArray[$index]['title'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'title', 'informations' ));
                        $this->datasArray[$index]['date'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'date', 'informations' ));
                        $this->datasArray[$index]['timestamp'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'timestamp', 'informations' ));
                        $this->datasArray[$index]['tags'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'tags', 'informations' ));
                        $this->datasArray[$index]['filename'] = trim(getItemIniFile( SP_DATAS.DS.$fichier, 'filename', 'informations' ));

                        $index ++;

                    }

                }

                // Fermeture du dossier    
                closedir( $dossier );
            }
        }else {

            // Il n'est pas possible d'accéder au répertoire Datas
            $this->status = 3;

        } 
    } // End function loadDatas()

    /**
    * Getter de la propriété $status
    * 
    */
    public function getStatus() {

        return $this->status;  

    } // End function getStatus()

    /**
    * Getter de la propriété $datasArray
    * 
    */
    public function getDatas() {

        return $this->datasArray;  

    } // End function getDatas()

    /**
    * Getter de la propriété $tags_Array
    * 
    */
    public function getTagsArray() {

        return $this->tags_array;  

    } // End function getTagsArray()

    /**
    * Getter de la propriété $lists_Array
    * 
    */
    public function getListsArray() {

        return $this->lists_array;  

    } // End function getListsArray()

    /**
    * Setter de la propriété datasArray
    * 
    * @param array $array
    */
    public function setDatas($array) {

        $this->datasArray = $array;  

    } // End function setDatas()

    /**
    * Méthode de qui recharge le tableau de données
    * 
    */
    public function reloadDatas() {

        $this->loadDatas($this->datasDirectory);

    }     


    /**
    * Métbode de tri du tableau de données
    * Prend en compte l'objet session pour afficher ou non les liens privés
    * 
    * @param array $array: Tableau à trier
    * @param string $key: La clé de tri
    * @param keyword $order: Le sens du tri (SORT_ASC ou SORT_DESC)
    * @param object $Session: L'objet Session
    * @return array $new_array:  Retourne le tableau trié
    */
    public function sortDatas($array, $key, $order = SORT_ASC, $Session, $filter = "aucun", $list = "ALL") {

        $new_array = array();
        $array_listed = array();
        $sortable_array = array();
        $tags_array = array();
        $tags_public_array = array();
        $tags_private_array = array();
        $lists_array = array();
        $lists_array_unique = array();
        $public_sortable_array = array();
        $privateFlag = FALSE;        
        $privateList = array();
        $publicList = array();
        $priv = 0;
        $pub = 0;


        if (count($array) > 0) {

            // Création du tableau sortable_array
            foreach ($array as $k => $v) {

                if (is_array($v)) {

                    foreach ($v as $k2 => $v2) {

                        // Prendre en compte la visibilité du Badge
                        if ($k2 == 'visibility' && $v2 == 'private')
                            $privateFlag = TRUE;
                        elseif ($k2 == 'visibility' && $v2 == 'public')
                            $privateFlag = FALSE;

                        if ($filter == 'aucun') {
                            if ($k2 == $key)
                                $sortable_array[$k] = strtoupper($v2);

                        }else{
                            if ($k2 == 'tags' && $v2 == $filter)
                                $sortable_array[$k] = strtoupper($v2);                                    
                        }


                        // Placer toutes les listes dans un tableau
                        if ($k2 == 'list')
                            $lists_array[$k] = $v2;

                        // Placer tous les tags dans un tableau
                        if ($k2 == 'tags')
                            $tags_array[$k] = $v2;


                        // Création d'une liste privée    
                        if ($privateFlag === TRUE) {

                            switch($k2) {

                                case 'filename': 
                                    $privateList[$priv] = $k;
                                    $priv++;
                                    break;
                                case 'tags':
                                    $tags_private_array[$k] = $v2;
                                    break;    
                            }

                            // Création d'une liste publique    
                        }elseif ($privateFlag === FALSE) {

                            switch($k2) {

                                case 'filename': 
                                    $publicList[$pub] = $k;
                                    $pub++;
                                    break;
                                case 'tags':
                                    $tags_public_array[$k] = $v2;
                                    break;    
                            }
                        }
                    }

                }else {
                    $sortable_array[$k] = $v;
                }
            }



            // Initialisation de la propriété $sortable_array dans un but de réutilisation
            $this->sortable_array = $sortable_array;


            // Si une session est ouverte on affiche l'entièreté du tableau
            if($Session->sessionOpen()) {

                switch ($order) {
                    case SORT_ASC:
                        $result = asort($sortable_array);
                        break;
                    case SORT_DESC:
                        $result = arsort($sortable_array);
                        break;
                }

                foreach ($sortable_array as $k => $v) {
                    $new_array[$k] = $array[$k];
                }

                // Suppression des doublons et tri ascendant du tableau $tags_array
                $tags_array = array_unique($tags_array);
                asort($tags_array);

                // Suppression des doublons et tri ascendant du tableau $lists_array
                $lists_array_unique = array_unique($lists_array);
                asort($lists_array_unique);

                // Initialisation de la propriété $tags_array
                $this->tags_array = $tags_array;

                // Initialisation de la propriété $lists_array
                $this->lists_array = $lists_array_unique;


                // Sinon on filtre avec l'aide de la publicList pour n'afficher que les liens publiques   
            }else{

                foreach($sortable_array as $sk => $sv)
                {
                    foreach($publicList as $pk => $pv) {

                        if($sk == $pv)
                        {
                            $public_sortable_array[$sk] = $sv; 
                        }
                    }

                }

                switch ($order) {
                    case SORT_ASC:
                        $result = asort($public_sortable_array);
                        break;
                    case SORT_DESC:
                        $result = arsort($public_sortable_array);
                        break;
                }

                foreach ($public_sortable_array as $k => $v) {
                    $new_array[$k] = $array[$k];
                }


                // Suppression des doublons et tri ascendant du tableau $tags_array
                $tags_public_array = array_unique($tags_public_array);
                asort($tags_public_array);
                
                 // Suppression des doublons et tri ascendant du tableau $lists_array
                $lists_array_unique = array_unique($lists_array);
                asort($lists_array_unique);

                // Initialisation de la propriété $tags_array 
                $this->tags_array = $tags_public_array;
                
                // Initialisation de la propriété $lists_array
                $this->lists_array = $lists_array_unique;

            }
            
            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
            // +++              Boucle de traitement des listes               +++  //
            // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //

            if($list != 'ALL')
            { 
                foreach($new_array as $k => $v)
                {
                    foreach($v as $k2 => $v2)
                    {
                        if ($k2 == 'list' && $v2 == $list)
                            $array_listed[$k] = $new_array[$k];
                    }
                }
            }else {
                // sinon  on copie tout simplement le contenu de new_array vers array_listed
                $array_listed = $new_array;
            }

        }

        return  $array_listed;


    }// End function sortDatas()

    /**
    * Fonction qui affiche les informations  
    * sous la forme d'un Badge
    *  
    * @param string $url : L'url du site
    * @param string $host :  Le Host du site
    * @param string $title :  Le titre du site
    * @param string $description : La description du site
    * @param string $bgThumb : Le background du Badge
    * @param string $textThumb :  Le texte affiché sur le Badge
    * @param string $date : La date de création du badge
    * @param string $tags : Les étiquettes du Badge
    * @param string $filename : Le nom du fichier (id)
    * @param string $Session : L'Objet Session
    * @return string : Retourne le Badge au format html
    */
    public function createBadge( $url, $list, $host, $title, $description, $bgThumb, $textThumb, $date, $tags, $visibility, $filename, $timestamp, $Session ) {

        $macaronUrl = $url;
        $macaronHost = substr($host, 0, 20);
        $dquotes = '"';
        //$textThumb = substr($textThumb, 0, 20);


        $stringAdmin='
        <div class="macaron ';
        if($tags == 'new') $stringAdmin .= $tags;
        $stringAdmin.=' badge-'.$bgThumb.'">
        <a class="text-center" href="'.$url.'" target="'.$_SESSION['target'].'">
        <div class="macaronThumb '.$bgThumb.'"><p class="textThumb">'.$textThumb.'</p></div>
        </a>
        <div class="macaronAdmin text-center">
        <a href="#" rel="popover" class="ancrePopover" title="Infos"><div class="macaronInfo popoverMacaron" data-placement="" data-original-title="'.$host.'" data-content="<strong>Description</strong>: '.substr($description, 0, 50).'...'.'<hr><strong>Titre</strong>: '.substr($title, 0 , 50).'...'.'<br><strong>Créé le</strong>: '.$date.'<br><strong>Url</strong>: '.$url.'<br><strong>Etiquette</strong>: '.$tags.'<br><strong>Visibilité</strong>: '.$visibility.'<hr><strong>Couleur du badge</strong>: '.$bgThumb.'<br><strong>Fichier</strong>: '.$filename.'<p class=\'text-center el_top10\'><span class=\' btn btn-default btn-xs closed\'>fermer</span></p>"><i class="fa fa-info fa-lg"></i></div></a>
        <a href="#" class="editBadge" data-id="'.$filename.'+S:E:P+'.$url.'+S:E:P+'.$list.'+S:E:P+'.$title.'+S:E:P+'.$description.'+S:E:P+'.$bgThumb.'+S:E:P+'.$textThumb.'+S:E:P+'.$tags.'+S:E:P+'.$host.'+S:E:P+'.$timestamp.'+S:E:P+'.$date.'+S:E:P+'.$visibility.'"><div class="macaronEdit" data-toggle="tooltip" data-placement="right" title="Edition du badge"><i class="fa fa-pencil fa-lg"></i></div></a>
        <a href="#" class="deleteBadge" data-id="'.$filename.'+S:E:P+'.$url.'"><div class="macaronDelete" data-toggle="tooltip" data-placement="right" title="Suppression du badge"><i class="fa fa-times fa-lg"></i></div></a>
        </div>
        <div class="macaronUrl">
        <a href="'.$url.'" target="'.$_SESSION['target'].'">'.$macaronHost.'</a>
        </div>
        </div>
        ';


        $stringConsult='
        <div class="macaron ';
        if($tags == 'new') $stringConsult .=$tags;
        $stringConsult.=' badge-'.$bgThumb.'">
        <a class="text-center" href="'.$url.'" target="'.$_SESSION['target'].'">
        <div class="macaronThumb '.$bgThumb.'"><p class="textThumb">'.$textThumb.'</p></div>
        </a>
        <div class="macaronAdmin text-center">
        <a href="#" rel="popover" class="ancrePopover"><div class="macaronInfo popoverMacaron" data-placement="" data-original-title="'.$host.'" data-content="<strong>Description</strong>: '.substr($description, 0, 50).'...'.'<hr><strong>Titre</strong>: '.substr($title, 0 , 50).'...'.'<br><strong>Créé le</strong>: '.$date.'<br><strong>Url</strong>: '.$url.'<br><strong>Etiquette</strong>: '.$tags.'<hr><strong>Couleur du badge</strong>: '.$bgThumb.'<p class=\'text-center el_top10\'><span class=\' btn btn-default btn-xs closed\'>fermer</span></p>"><i class="fa fa-info fa-lg"></i></div></a>
        </div>
        <div class="macaronUrl">
        <a href="'.$url.'" target="'.$_SESSION['target'].'">'.$macaronHost.'</a>
        </div>
        </div>
        ';

        if($Session->sessionOpen())
            return $stringAdmin;
        else
            return $stringConsult;


    }// End function createBadge()

    /**
    * Fonction qui affiche les informations  
    * sous la forme de ligne insérées dans un tableau
    *  
    * @param string $url : L'url du site
    * @param string $host :  Le Host du site
    * @param string $title :  Le titre du site
    * @param string $description : La description du site
    * @param string $bgThumb : Le background du Badge
    * @param string $textThumb :  Le texte affiché sur le Badge
    * @param string $date : La date de création du badge
    * @param string $tags : Les étiquettes du Badge
    * @param string $filename : Le nom du fichier (id)
    * @param string $Session : L'Objet Session
    * @return string : Retourne la ligne(row) au format html
    */
    public function createRow( $nbThumb, $path, $urlRead, $listRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $timestampRead, $Session ) {

        // Shortified string fields
        $shortUrlRead = substr($urlRead, 0, 50);
        $descriptionReadShort = substr($descriptionRead, 0, 75);
        $titleReadShort = substr($titleRead, 0, 40);

        $tbodyConsult =  '<tr class="tdBody">
        <td class="verticalAllign text-center">'.$nbThumb.'</td>
        <td class="verticalAllign text-center hidden-xs"><img src="'.$path.'dot-'.$bgThumbRead.'.png"></td>
        <td class="magnify"><a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a><br><small>'.$urlRead.'</small></td>
        <td class="verticalAllign hidden-xs">'.$titleRead.'</td>
        <td class="verticalAllign hidden-xs">'.$descriptionRead.'</td>
        <td class="verticalAllign text-center hidden-xs">'.$tagsRead.'</td>
        </tr>';

        $tbodyAdmin =  '<tr class="tdBody">
        <td class="verticalAllign text-center">'.$nbThumb.'</td>
        <td class="verticalAllign text-center hidden-xs"><img src="'.$path.'dot-'.$bgThumbRead.'.png"></td>
        <td class="magnify"><a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a><br><small>'.$urlRead.'</small></td>
        <td class="verticalAllign hidden-xs">'.$titleReadShort.'</td>
        <td class="verticalAllign hidden-xs">'.$descriptionReadShort.'</td>
        <td class="verticalAllign text-center hidden-xs">'.$tagsRead.'</td>
        <td class="verticalAllign text-center"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'" class="deleteBadge"><button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i></button></a></td>
        <td class="verticalAllign text-center"><a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'+S:E:P+'.$listRead.'+S:E:P+'.$titleRead.'+S:E:P+'.$descriptionRead.'+S:E:P+'.$bgThumbRead.'+S:E:P+'.$textThumbRead.'+S:E:P+'.$tagsRead.'+S:E:P+'.$hostRead.'+S:E:P+'.$timestampRead.'+S:E:P+'.$dateRead.'+S:E:P+'.$visibilityRead.'" class="editBadge"><button type="submit" class="btn btn-success"><i class="fa fa-pencil"></i></button></a></td>
        </tr>';

        if($Session->sessionOpen())
            return $tbodyAdmin;
        else
            return $tbodyConsult;


    }// End function createRow()

    /**
    * Fonction qui affiche les informations  
    * sous la forme de ligne insérées dans un tableau
    *  
    * @param string $url : L'url du site
    * @param string $host :  Le Host du site
    * @param string $title :  Le titre du site
    * @param string $description : La description du site
    * @param string $bgThumb : Le background du Badge
    * @param string $textThumb :  Le texte affiché sur le Badge
    * @param string $date : La date de création du badge
    * @param string $tags : Les étiquettes du Badge
    * @param string $filename : Le nom du fichier (id)
    * @param string $Session : L'Objet Session
    * @return string : Retourne la ligne(row) au format html
    */
    public function createList( $nbThumb, $path, $urlRead, $listRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $timestampRead, $Session ) {

        // Icône de visibilité en fontion de l'état
        if($visibilityRead == 'public')
            $iconVis = '<i class="fa fa-unlock-alt"></i>';
        else
            $iconVis = '<i class="fa fa-lock"></i>';

        // Mettre en évidence un nouveau badge ajouté en modifiant la classe du tag 
        if($tagsRead == 'new')
            $classTags = 'btnRed';
        else
            $classTags = 'btnGreen';


        $itemAdmin = '        
        <p class="itemList">
        <span class="KTbtn hidden-xs">'.$nbThumb.'</span>
        <span class="KTbtn item'.$visibilityRead.' hidden-xs" data-toggle="tooltip" data-placement="right" title="'.$visibilityRead.'">'.$iconVis.'</span>
        <span class="hidden-xs" data-toggle="tooltip" data-placement="right" title="'.$bgThumbRead.'"><img src="'.$path.'dot-'.$bgThumbRead.'.png"></span>
        <a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'" class="deleteBadge"><span class="KTbtn itemDelete hidden-xs" data-toggle="tooltip" data-placement="right" title="Suppression"><i class="fa fa-trash-o"></i></span></a>
        <a href="#" data-id="'.$filenameRead.'+S:E:P+'.$urlRead.'+S:E:P+'.$listRead.'+S:E:P+'.$titleRead.'+S:E:P+'.$descriptionRead.'+S:E:P+'.$bgThumbRead.'+S:E:P+'.$textThumbRead.'+S:E:P+'.$tagsRead.'+S:E:P+'.$hostRead.'+S:E:P+'.$timestampRead.'+S:E:P+'.$dateRead.'+S:E:P+'.$visibilityRead.'" class="editBadge"><span class="KTbtn itemEdit hidden-xs" data-toggle="tooltip" data-placement="down" title="Edition"><i class="fa fa-pencil"></i></span></a>&nbsp;
        <a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a>&nbsp;
        <a href="index.php?view='.$_SESSION['view'].'&filter='.$tagsRead.'" title="filtrer"><span class="btn KTbtn '.$classTags.' hidden-xs">'.$tagsRead.'</span></a>
        </p>
        ';

        $itemConsult = '
        <p class="itemList">
        <span class="KTbtn hidden-xs">'.$nbThumb.'</span>
        <a href="'.$urlRead.'" target="'.$_SESSION['target'].'">'.$textThumbRead.'</a>
        <a href="index.php?view='.$_SESSION['view'].'&filter='.$tagsRead.'" title="filtrer"><span class="btn KTbtn '.$classTags.' hidden-xs">'.$tagsRead.'</span></a>
        </p>
        ';                    

        if($Session->sessionOpen())
            return $itemAdmin;
        else
            return $itemConsult;


    }// End function createRow()

    public function displayMessageStatus($statusId) {

        switch($statusId) {

            case 1:
                return '';
            case 2:
                return '<div class="row text-center"><div class="col-md-3"></div><div id="emptyDir" class="col-md-6 Kt_color_2">C\'est un peu vide ici...<br>Il est temps pour vous d\'ajouter un nouveau Lien !</div><div class="col-md-3"></div></div>';
            case 3:
                return '<div class="row text-center"><div class="col-md-3"></div><div id="emptyDir" class="col-md-6"><p><span class="btn btn-danger">Avertissement</span></p><p class="msg">L\'application ne peut accéder au répertoire de stockage des données: <span class="el_red">'.$this->datasDirectory.'</span></p><p class="msg">Veuillez vérifier son existence ainsi que les droits d\'accès de ce dernier</p></div><div class="col-md-3"></div></div>';
        }    
    }


} //End Class Datas