<?php

/**
* KT START - Class Session
* Gestion et manipulation des Sessions
* 
* Author: ken@kentaro.be  - www.ktdev.info 
* Under Licence MIT
*/



class Session{

    var $paramsArray = array();
    var $fileParameters = NULL;
    var $initializeToken = NULL;

    public function __construct() {

        // Lancement de la Session
        session_start();

        // Chargement des paramètres
        //$this->loadParams($this->fileParameters);

    }

    /**
    * Méthode de chargement des paramètres
    * 
    */
    private function loadParams() {

        // Initialisation
        $this->fileParameters = SP_PARAMS;

        // Initialisations
        $this->paramsArray['VISIBILITY'] = trim( getItemIniFile( $this->fileParameters, 'VISIBILITY', 'parameters' ) );      
        $this->paramsArray['DEFAULT_LIST'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_LIST', 'parameters' ) );      
        $this->paramsArray['URLAPP'] = trim( getItemIniFile( $this->fileParameters, 'URLAPP', 'parameters' ) );
        $this->paramsArray['DEFAULT_VIEW'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_VIEW', 'parameters' ) );   
        $this->paramsArray['TARGET'] = trim( getItemIniFile( $this->fileParameters, 'TARGET', 'parameters' ) );
        $this->paramsArray['DEFAULT_THUMB'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_THUMB', 'parameters' ) );
        $this->paramsArray['DEFAULT_SORT'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_SORT', 'parameters' ) );      
        $this->paramsArray['DEFAULT_ORDER'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_ORDER', 'parameters' ) );      
        $this->paramsArray['DEFAULT_TAG'] = trim( getItemIniFile( $this->fileParameters, 'DEFAULT_TAG', 'parameters' ) );      


        // Post traitements
        if( $this->paramsArray['DEFAULT_ORDER'] == 'SORT_ASC' )
            $this->paramsArray['DEFAULT_ORDER'] = SORT_ASC;
        else
            $this->paramsArray['DEFAULT_ORDER'] = SORT_DESC;

        // Mise en session des paramètres
        $_SESSION['visibility'] = $this->paramsArray['VISIBILITY'];
        $_SESSION['list'] = $this->paramsArray['DEFAULT_LIST'];
        $_SESSION['urlApp'] = $this->paramsArray['URLAPP'];
        $_SESSION['target'] = $this->paramsArray['TARGET'];
        $_SESSION['defaultBgThumb'] = $this->paramsArray['DEFAULT_THUMB'];
        $_SESSION['defaultSort'] = $this->paramsArray['DEFAULT_SORT'];
        $_SESSION['defaultOrder'] = $this->paramsArray['DEFAULT_ORDER'];
        $_SESSION['defaultTag'] = $this->paramsArray['DEFAULT_TAG'];

        // Intialisation si nécessaire de  view, sort, order
        if( empty( $_SESSION['view'] ) )
            $_SESSION['view'] = $this->paramsArray['DEFAULT_VIEW'];

        if( empty( $_SESSION['sort'] ) )
            $_SESSION['sort'] = $this->paramsArray['DEFAULT_SORT'];

        if( empty( $_SESSION['order'] ) )
            $_SESSION['order'] = $this->paramsArray['DEFAULT_ORDER'];



    }

    /**
    * Méthode de qui recharge le tableau de paramètres
    * 
    */
    public function reloadParams() {

        $this->loadParams();

    }   

    /**
    * Getter des entrées de la  propriété paramsArray
    * 
    * @param string $parameter
    */
    public function getParams($parameter) {
        switch($parameter) {

            case 'URLAPP':
                return $this->paramsArray['URLAPP'];
            case 'DEFAULT_VIEW':
                return $this->paramsArray['DEFAULT_VIEW'];
            case 'TARGET':
                return $this->paramsArray['TARGET'];
            case 'DEFAULT_THUMB':
                return $this->paramsArray['DEFAULT_THUMB'];
            case 'DEFAULT_SORT':
                return $this->paramsArray['DEFAULT_SORT'];
            case 'DEFAULT_ORDER':
                if( $this->paramsArray['DEFAULT_ORDER'] == SORT_ASC )
                    return 'SORT_ASC';
                if( $this->paramsArray['DEFAULT_ORDER'] == SORT_DESC )
                    return 'SORT_DESC';
            case 'DEFAULT_TAG':
                return $this->paramsArray['DEFAULT_TAG'];
            case 'VISIBILITY':
                return $this->paramsArray['VISIBILITY'];
            case 'LIST':
                return $this->paramsArray['DEFAULT_LIST']; 
        }
    }

    /**
    * Setter de la propriété paramsArray
    * 
    * @param string $type
    * @param string $parameter
    */
    public function setParams($type, $parameter) {

        switch($type) {

            case 'VISIBILITY':
                $this->paramsArray['VISIBILITY'] = $parameter; 
                break;
            case 'DEFAULT_LIST':
                $this->paramsArray['DEFAULT_LIST'] = $parameter; 
                break;
            case 'URLAPP':
                $this->paramsArray['URLAPP'] = $parameter;
                break;
            case 'DEFAULT_VIEW':
                $this->paramsArray['DEFAULT_VIEW'] = $parameter;
                break;
            case 'TARGET':
                $this->paramsArray['TARGET'] = $parameter;
                break;
            case 'DEFAULT_THUMB':
                $this->paramsArray['DEFAULT_THUMB'] = $parameter;
                break;
            case 'DEFAULT_SORT':
                $this->paramsArray['DEFAULT_SORT'] = $parameter;
                break;
            case 'DEFAULT_ORDER':
                $this->paramsArray['DEFAULT_ORDER'] = $parameter;
                break;
            case 'DEFAULT_TAG':
                $this->paramsArray['DEFAULT_TAG'] = $parameter;
                break;

        }
    }

    /**
    * Getter de la propriété $paramArray
    * 
    */
    public function getParamsArray() {
        return $this->paramsArray;
    }

    public function sessionOpen()
    {
        if(isset($_SESSION['identified']) && $_SESSION['identified'] === TRUE)
            return TRUE;
        else
            return FALSE;
    }

    /**
    * Vérifie l'identité de l'utilisateur lors de l'identification
    * 
    * @param string $passwdK
    * @param string $token 
    * @param string $userProfile 
    * @param string $passwordProfile 
    */
    function matchPasswd($passwordK, $token, $userProfile, $passwordProfile)
    {
        $password = crp::decrypte($passwordK, $token);
        $passwordProfile = crp::decrypte($passwordProfile, $userProfile);
        
			if($password == $passwordProfile)
			{
				$_SESSION['identified'] = TRUE;
			}else{
				$_SESSION['identified'] = FALSE;
			}
    } 



    public function setFlash($message, $type = 'error') {
        $_SESSION['flash'] = array(
            'message' => $message,
            'type'      => $type
        );
    }

    public function flash() {
        if(isset( $_SESSION['flash'] )){

            $string='
            <div class="col-md-3"></div>
            <div id="alert" class="row text-center alert alert-dismissable alert-'.$_SESSION['flash']['type'].' col-md-6" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            '.$_SESSION['flash']['message'].'
            </div> 
            <div class="col-md-3"></div>
            ';            

            return $string;

        }
    }

    public function unsetFlash() {
        unset($_SESSION['flash']);    
    }


    private function getParam($file, $item, $group)
    {
        $valeur = false;
        if(file_exists($file) && $readFile = file($file))
        {
            foreach($readFile as $row)
            {
                $cleanRow = trim($row);
                if(preg_match( "#^\[(.+)\]$#", $cleanRow, $matches ))
                {
                    $groupMatched = $matches[1];
                }else{
                    if($groupMatched == $group)
                    {
                        if(strpos($row, $item."=")=== 0)
                            $valeur = end( explode( "=", $row, 2 ));
                        elseif($row == $item)
                            $valeur = '';
                    }
                }
            }
        }else{
            return 'PARAMETER ERROR';
        }

        if($valeur === FALSE)
            return FALSE;  // Groupe ou item inexistant
        else
            return $valeur;

    }

    /**
    * Retourne le phpinfo du serveur
    * @return mixed $infos
    */
    public function getInfosServer() {
        ob_start();
        phpinfo(); 
        $infos = ob_get_contents();
        ob_end_clean();
        $infos = preg_replace( '%^.*<body>(.*)</body>.*$%ms','$1',$infos);
        return $infos;
    }

    /**
    * Enregistre la session (sérialisé) en cours
    * 
    * @param mixed $Sess
    * @return boolean 
    */
    public function storeSession($Sess) {
        $fp = fopen(SP_CORE.DS.SP_STORE.DS.'session-serialized', "w");
        if($fp !== FALSE) {
            fwrite($fp, $Sess);
            fclose($fp);
            return TRUE;   
        }else
            return FALSE; 
    }

    /**
    * Getter de la propriété $initializeToken
    * 
    */
    public function getinitToken() {
        return $this->initializeToken;
    }

    /**
    * Setter de la propriété $initializeToken
    * 
    */
    public function setinitToken($token) {
        $this->initializeToken = $token;
    }

    /**
    * Initialise les paramètres par défaut
    * 
    */
    public static function defaultParams($url) {

        $paramsArray['VISIBILITY'] = 'private';
        $paramsArray['DEFAULT_LIST'] = 'FAVORIS';
        $paramsArray['URLAPP'] = $url;
        $paramsArray['DEFAULT_VIEW'] = 'BADGESVIEW';
        $paramsArray['TARGET'] = '_blank';
        $paramsArray['DEFAULT_THUMB'] = 'lightBlue';
        $paramsArray['DEFAULT_SORT'] = 'textThumb';
        $paramsArray['DEFAULT_ORDER'] = 'SORT_ASC';
        $paramsArray['DEFAULT_TAG'] = 'new';
        
        return $paramsArray;

    }



}
