 /**
    * KT START - kt-start.js 
    * 
    * Author: ken@kentaro.be  - www.ktdev.info 
    * Under Licence MIT
    */

/* ++++++++++++++++++++++++++++++ */
/* +++ Traitement des PopOver +++ */
/* ++++++++++++++++++++++++++++++ */
$('.popoverMacaron').popover({ 
    html: true
}).click(function (e) {
    $(this).popover('show');
    $('.popover-content').append('<a class="closed times" style="position: absolute; top: 0; right: 6px;">&times;</a>');
    clickedAway = false
    isVisible = true
    e.preventDefault()
});

$('.popoverMacaron').click(function (e) {
    e.stopPropagation();
});

$(document).click(function (e) {
    if (($("[rel='popover']").has(e.target).length == 0) || $(e.target).is('.closed')) {
        $('.popoverMacaron').popover('hide');
    }
});

/* +++++++++++++++++++++++++++++++ */
/* +++ Traitement des Tooltips +++ */
/* +++++++++++++++++++++++++++++++ */
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


/* ++++++++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement du message d'alerte Flash +++ */
/* ++++++++++++++++++++++++++++++++++++++++++++ */
$(function() {

    var alert = $('#alert'); 
    if(alert.length > 0){
        alert.hide().slideDown(500).delay(5000).slideUp();
        alert.find('.close').click(function(e){
            e.preventDefault();
            alert.slideUp();
        })
    }

});


/* ++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale Delete +++ */
/* ++++++++++++++++++++++++++++++++++++++ */
$('.deleteBadge').click(function(){
    /* Vidage de l'url avant l'écriture pour éviter les multis */
    $('#delete-modal #lien').empty();

    /* Ob capture la chaîne de caractères de l'attribut data-id */
    var datas = $(this).attr('data-id');

    /* On split les données de la chaîne sur la virgule */ 
    var splittedVars = datas.split('+S:E:P+');

    /* On initialise les variables avec le tableau splitted */ 
    var id = splittedVars[0];
    var url = splittedVars[1];

    /* On écrit les données dans la modale */ 
    $('#delete-modal #id').val(id);
    $('#delete-modal #lien').attr("href", url);
    $('#delete-modal #lien').append(url);

    /* On affiche la Modale Delete */
    $('#delete-modal').modal('show');

});
/* ++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale Edit +++ */
/* ++++++++++++++++++++++++++++++++++++ */
$('.editBadge').click(function(){

    /* On capture la chaîne de caractères de l'attribut data-id */
    var datas = $(this).attr('data-id');

    /* On split les données de la chaîne sur la chaine +SEP+ */ 
    var splittedVars = datas.split('+S:E:P+'); 

    /* On initialise les variables avec le tableau splitted */ 
    var id = splittedVars[0];
    var url = splittedVars[1];
    var list = splittedVars[2];
    var titre = splittedVars[3];
    var description = splittedVars[4];
    var bgThumb = splittedVars[5];
    var textThumb = splittedVars[6];
    var tags = splittedVars[7];
    var host = splittedVars[8];
    var timestamp = splittedVars[9];
    var date = splittedVars[10];
    var visibility = splittedVars[11];

    /* Suppressions et retraits */
    $('#edit-modal #lien').empty();
    $('#textBgThumb').empty();

    /* On écrit les données dans la modale */ 
    $('#edit-modal #lien').attr("href", url);
    $('#edit-modal #lien').append(url);
    $('#edit-modal #id').val(id);
    $('#edit-modal #titre').val(titre);
    $('#edit-modal #description').val(description);
    $('#edit-modal #textBgThumb').append(bgThumb);
    $('#edit-modal #textThumb').val(textThumb);
    $('#edit-modal #tags').val(tags);
    $('#edit-modal #host').val(host);
    $('#edit-modal #timestamp').val(timestamp);
    $('#edit-modal #date').val(date);
    $('#edit-modal #url').val(url);
    $('#edit-modal #ktlist').val(list);

    // Sélection de l'option du select en fonction de la valeur de bgThumb
    switch(bgThumb) {
        case 'lightBlue': $(".selectpicker").selectpicker('val', 'lightBlue');
            break;
        case 'deepBlue': $(".selectpicker").selectpicker('val', 'deepBlue');
            break;
        case 'grassGreen': $(".selectpicker").selectpicker('val', 'grassGreen');
            break;
        case 'lightGreen': $(".selectpicker").selectpicker('val', 'lightGreen');
            break;
        case 'lightRed': $(".selectpicker").selectpicker('val', 'lightRed');
            break;
        case 'turquoise': $(".selectpicker").selectpicker('val', 'turquoise');
            break;
        case 'blueSky': $(".selectpicker").selectpicker('val', 'blueSky');
            break;
        case 'camouflage': $(".selectpicker").selectpicker('val', 'camouflage');
            break;
        case 'deepPurple': $(".selectpicker").selectpicker('val', 'deepPurple');
            break;
        case 'lightOrange': $(".selectpicker").selectpicker('val', 'lightOrange');
            break;
        case 'lightViolet': $(".selectpicker").selectpicker('val', 'lightViolet');
            break;
        case 'lightYellow': $(".selectpicker").selectpicker('val', 'lightYellow');
            break;        
    }


    $('select.selectpicker').on('change', function() {
        /*var selectedValue = $('select.selectpicker [name=selValue]').val();*/
        /*$('.selectpicker').selectpicker('refresh');*/

        /* var selectedValue = $('select.selectpicker [name=selValue]').val(1);*/
        /*console.log('CHANGE: '+selectedValue);*/

        $('#textBgThumb').empty();
        /*$('#edit-modal #textBgThumb').append(selectedValue);    */
    });

    // Sélection de l'option du select en fonction de la valeur de visibility
    switch(visibility) {
        case 'public': $("select#visibility").prop('selectedIndex', 0);
            break;
        case 'private': $("select#visibility").prop('selectedIndex', 1);
            break;
    }

    /* On affiche la Modale Delete */
    $('#edit-modal').modal('show');

});

/* +++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale de Tris +++ */
/* +++++++++++++++++++++++++++++++++++++++ */
$('.sortViews').click(function(){

    // On capture la chaîne de caractères de l'attribut data-id 
    var datas = $(this).attr('data-id');

    // On split les données de la chaîne sur la chaine +SEP+ 
    var splittedVars = datas.split('+S:E:P+'); 

    // On initialise les variables avec le tableau splitted 
    var sort = splittedVars[0];
    var order = splittedVars[1];

    // Sélection de l'option du select en fonction de la valeur de bgThumb
    switch(sort) {
        case 'tags': $(".selectpicker").selectpicker('val', 'tags');
            break;
        case 'textThumb': $(".selectpicker").selectpicker('val', 'textThumb');
            break;
        case 'bgThumb': $(".selectpicker").selectpicker('val', 'bgThumb');
            break;
        case 'timestamp': $(".selectpicker").selectpicker('val', 'timestamp');
            break;
        case 'url': $(".selectpicker").selectpicker('val', 'url');
            break;
        case 'host': $(".selectpicker").selectpicker('val', 'host');
            break;
        case 'title': $(".selectpicker").selectpicker('val', 'title');
        break;

    }

    //3: DESC || 4: ASC
    switch(order) {
        case '3': $("#direction_desc").attr('checked', 'checked'); 
            break;
        case '4': $("#direction_asc").attr('checked', 'checked'); 
            break;

    }


    /* On affiche la Modale de Tris */
    $('#sort-modal').modal('show');

});

/* +++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale de Tris +++ */
/* +++++++++++++++++++++++++++++++++++++++ */
$('.filterViews').click(function(){

    // On capture la chaîne de caractères de l'attribut data-id 
    var datas = $(this).attr('data-id');

    // On split les données de la chaîne sur la chaine +SEP+ 
    var splittedVars = datas.split('+S:E:P+'); 

    // On initialise les variables avec le tableau splitted 
    
   


    /* On affiche la Modale de Tris */
    $('#filter-modal').modal('show');

});

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++ Traitement de la Modale d'édition des paramètres +++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
$('.modalParams').click(function(){

    /* On capture la chaîne de caractères de l'attribut data-id */
    var datas = $(this).attr('data-id');

    /* On split les données de la chaîne sur la chaine +SEP+ */ 
    var splittedVars = datas.split('+S:E:P+'); 

    /* On initialise les variables avec le tableau splitted */ 
    var url = splittedVars[0];
    var view = splittedVars[1];
    var target = splittedVars[2]
    var badge = splittedVars[3];
    var sort = splittedVars[4];
    var order = splittedVars[5];
    var tags = splittedVars[6];
    var visibility = splittedVars[7];

    // Ecrit la valeur du paramètre url dans l'attribut value de de l'input
    $('#params-modal #urlAppParam').val(url);

    // Sélection de l'option du select en fonction de la valeur de view
    switch(view) {
        case 'BADGEVIEW': $("select#defaultViewParam").prop('selectedIndex', 0);
            break;
        case 'ARRAYVIEW': $("select#defaultViewParam").prop('selectedIndex', 1);
            break;
        case 'LISTVIEW': $("select#defaultViewParam").prop('selectedIndex', 2);
        break;
    }

    // Sélection de l'option du select en fonction de la valeur de target
    switch(target) {
        case '_blank': $("select#targetParam").prop('selectedIndex', 0);
            break;
        case '_self': $("select#targetParam").prop('selectedIndex', 1);
        break;
    }

    // Sélection de l'option du select en fonction de la valeur de badge
    switch(badge) {
        case 'lightBlue': $("#bgThumbParam").selectpicker('val', 'lightBlue');
            break;
        case 'deepBlue': $("#bgThumbParam").selectpicker('val', 'deepBlue');
            break;
        case 'grassGreen': $("#bgThumbParam").selectpicker('val', 'grassGreen');
            break;
        case 'lightGreen': $("#bgThumbParam").selectpicker('val', 'lightGreen');
            break;
        case 'lightRed': $("#bgThumbParam").selectpicker('val', 'lightRed');
            break;
        case 'turquoise': $("#bgThumbParam").selectpicker('val', 'turquoise');
            break;
        case 'blueSky': $("#bgThumbParam").selectpicker('val', 'blueSky');
            break;
        case 'camouflage': $("#bgThumbParam").selectpicker('val', 'camouflage');
            break;
        case 'deepPurple': $("#bgThumbParam").selectpicker('val', 'deepPurple');
            break;
        case 'lightOrange': $("#bgThumbParam").selectpicker('val', 'lightOrange');
            break;
        case 'lightViolet': $("#bgThumbParam").selectpicker('val', 'lightViolet');
            break;
        case 'lightYellow': $("#bgThumbParam").selectpicker('val', 'lightYellow');
        break;        
    }


    // Sélection de l'option du select en fonction de la valeur de sort
    switch(sort) {
        case 'tags': $("#sortParam").selectpicker('val', 'tags');
            break;
        case 'textThumb': $("#sortParam").selectpicker('val', 'textThumb');
            break;
        case 'bgThumb': $("#sortParam").selectpicker('val', 'bgThumb');
            break;
        case 'timestamp': $("#sortParam").selectpicker('val', 'timestamp');
            break;
        case 'url': $("#sortParam").selectpicker('val', 'url');
            break;
        case 'host': $("#sortParam").selectpicker('val', 'host');
            break;
        case 'title': $("#sortParam").selectpicker('val', 'title');
        break;

    }

    // Sélection de l'option du select en fonction de la valeur d'order
    switch(order) {
        case 'SORT_ASC': $("select#orderParam").prop('selectedIndex', 0);
            break;
        case 'SORT_DESC': $("select#orderParam").prop('selectedIndex', 1);
        break;
    }

    // Ecrit la valeur du paramètre tags dans l'attribut value de de l'input
    $('#params-modal #tagsParam').val(tags);

    // Sélection de l'option du select en fonction de la valeur de visibility
    switch(visibility) {
        case 'public': $("select#visibilityParam").prop('selectedIndex', 0);
            break;
        case 'private': $("select#visibilityParam").prop('selectedIndex', 1);
            break;
    }


    /* On affiche la Modale d'édition des paramètres */
    $('#params-modal').modal('show');

});


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++++++++ Affiche la fenêtre modale des filtres ++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
$('.filterViews').click(function(){
    $('#filter-modal').modal('show');

});

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++ Affiche la fenêtre modale des infos +++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
$('.apropos').click(function(){
    $('#infos-modal').modal('show');

});

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++++++++ Affiche la fenêtre modale des outils +++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
$('.modalTools').click(function(){
    $('#tools-modal').modal('show');

});

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* +++++++++++++++++++ IDENTIFICATION +++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
$( "#formLogin" ).submit(function( event ) {

    var passwd = $("#password").val();
    var token = $("#tokenForm").val();

    // Cryptage des données
    var passwdK = $.crp.crypte(passwd, token);
    //var tokenK = $.crp.crypte(token, token);
       
    // Initialisation des inputs hidden
    $('#formLogin #passwordK').val(passwdK);
    //$('#formLogin #tokenK').val(tokenK);
        
    // Effacer les champs visible du formulaire
    $('#formLogin #password').val('');
        
    /*alert(
        'PASS '+passwd+ '\n '+'PASSCONF '+passwdConfirm+ '\n '+'EMAIL '+email+ '\n '+'EMAILCONF '+emailConfirm+ '\n '+'QUEST '+question+ '\n '+'REP '+reponse+ '\n '+'TOKEN '+token+'\n\n\n'+
        'PASSK '+passwdK+ '\n '+'PASSCONFK '+passwdConfirmK+ '\n '+'EMAILK '+emailK+ '\n '+'EMAILCONFK '+emailConfirmK+ '\n '+'QUESTK '+questionK+ '\n '+'REPK '+reponseK+ '\n '+'TOKENK '+tokenK    
    );*/


});


  



