
$(document).ready(function(){
    $("#formInit").validate();
});

$( "#formInit" ).validate({
    rules: {

         username: {
            required: true,
            minlength: 3
        },
        password: {
            required: true,
            minlength: 6
        },
        passwordConfirm: {
            required: true,
            minlength: 6,
            equalTo: "#password"
        },
        email: {
            required: true,
            email: true
        },
        emailConfirm: {
            required: true,
            email: true,
            equalTo: "#email"
        },
        reponse: {
            required: true
        }


    },
    messages: {
               
                passwordConfirm: {
                   equalTo: "Les deux mots de passe doivent être identique"
                },
                emailConfirm: {
                   equalTo: "Les deux adresses e-mail doivent être identique"
                }
            }

});

$( "#formInit" ).submit(function( event ) {


    var username = $("#username").val();
    var passwd = $("#password").val();
    var passwdConfirm = $("#passwordConfirm").val();
    var email = $("#email").val();
    var emailConfirm = $("#emailConfirm").val();
    var question = $("#question").val();
    var reponse = $("#reponse").val();
    var token = $("#tokenForm").val();



    // Cryptage des données
    var usernameK = $.crp.crypte(username, token);
    var passwdK = $.crp.crypte(passwd, token);
    var passwdConfirmK = $.crp.crypte(passwdConfirm, token);
    var emailK = $.crp.crypte(email, token);
    var emailConfirmK = $.crp.crypte(emailConfirm, token);
    var questionK = $.crp.crypte(question, token);
    var reponseK = $.crp.crypte(reponse, token);
    var tokenK = $.crp.crypte(token, token);

    // Initialisation des inputs hidden
    $('#formInit #usernameK').val(usernameK);
    $('#formInit #passwordK').val(passwdK);
    $('#formInit #passwordConfirmK').val(passwdConfirmK);
    $('#formInit #emailK').val(emailK);
    $('#formInit #emailConfirmK').val(emailConfirmK);
    $('#formInit #questionK').val(questionK);
    $('#formInit #reponseK').val(reponseK);
    $('#formInit #tokenK').val(tokenK);

    // Effacer les champs visible du formulaire
    $('#formInit #username').val('');
    $('#formInit #password').val('');
    $('#formInit #passwordConfirm').val('');
    $('#formInit #email').val('');
    $('#formInit #emailConfirm').val('');
    $('#formInit #question').val(0);
    $('#formInit #reponse').val('');
    $('#formInit #token').val('');

    /*alert(
    'PASS '+passwd+ '\n '+'PASSCONF '+passwdConfirm+ '\n '+'EMAIL '+email+ '\n '+'EMAILCONF '+emailConfirm+ '\n '+'QUEST '+question+ '\n '+'REP '+reponse+ '\n '+'TOKEN '+token+'\n\n\n'+
    'PASSK '+passwdK+ '\n '+'PASSCONFK '+passwdConfirmK+ '\n '+'EMAILK '+emailK+ '\n '+'EMAILCONFK '+emailConfirmK+ '\n '+'QUESTK '+questionK+ '\n '+'REPK '+reponseK+ '\n '+'TOKENK '+tokenK    
    );*/

    return valid;


});