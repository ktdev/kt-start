<?php

/**
* KT START
* Author: ken@kentaro.be  - www.ktdev.info 
* Under Licence MIT
*/


/**
* Retourne la string du Menu principal de KT Start
* 
* @param string $menuActif
* @param object $Session
*/
function displayMenu($menuActif = NULL, $Session)
{
    $classBadges = NULL;
    $classList = NULL; 
    $classArray = NULL;
    $classDebug = NULL; 
    $adminSection = NULL;
    $liFilter = NULL;
    $classActive= 'class="active"';

    if($_SESSION['filter'] == 'aucun')
        $liFilter = '<li><a href="#" class="filterName">&nbsp;filtre inactif</a></li>';
    else
    $liFilter = '<li><a href="index.php?view='.$_SESSION['view'].'&filter=aucun" data-id="" class="filterItem" title="Supprimer le filtre"><i class="fa fa-trash-o"></i>&nbsp; '.$_SESSION['filter'].'</a></li>';



    switch($menuActif)
    {
        case 'BADGESVIEW':  
            $classBadges = $classActive;
            break;
        case 'ARRAYVIEW':    
            $classArray = $classActive;
            break;
        case 'LISTVIEW':    
            $classList = $classActive;
            break;
        case 'DEBUGVIEW':    
            $classDebug = $classActive;
            break;

    }

    if($Session->sessionOpen())
        $adminSection = '
        <li fa><a href="#" title="Paramètres" data-id="'.$Session->getParams('URLAPP').'+S:E:P+'.$Session->getParams('DEFAULT_VIEW').'+S:E:P+'.$Session->getParams('TARGET').'+S:E:P+'.$Session->getParams('DEFAULT_THUMB').'+S:E:P+'.$Session->getParams('DEFAULT_SORT').'+S:E:P+'.$Session->getParams('DEFAULT_ORDER').'+S:E:P+'.$Session->getParams('DEFAULT_TAG').'+S:E:P+'.$Session->getParams('VISIBILITY').'" class="modalParams filterItem"><i class="fa fa-cog"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Paramètres</span></a></li>
        <li fa><a href="#" title="Outils" data-id="" class="modalTools filterItem"><i class="fa fa-wrench"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Outils</span></a></li> 
        <li fa><a href="logout.php" title="Déconnexion" class="modalParams filterItem"><i class="fa fa-sign-out"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Déconnexion</span></a></li>
        <li class="divider-vertical hidden-xs"></li>
        ';

    $menu = '

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="'.$_SESSION['urlApp'].'"><img src="'.SP_CORE.DS.SP_IMG.DS.'KT-Start2.png" alt="Logo KT-Start"></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
    <ul class="nav navbar-nav nav-ktstart fa-ul">
    <li '.$classBadges.' fa><a href="index.php?view=BADGESVIEW" title="Vue Badge"><i class="fa fa-th-large"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Vue Badge</span></a></li> 
    <li '.$classArray.' fa><a href="index.php?view=ARRAYVIEW" title="Vue Tableau"><i class="fa fa-table"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Vue Tableau</span></a></li>
    <li '.$classList.' fa><a href="index.php?view=LISTVIEW" title="Vue Liste"><i class="fa fa-th-list"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Vue Liste</span></a></li>
    <li class="divider-vertical hidden-xs"></li>
    <li fa><a href="#" title="Trier" data-id="'.$_SESSION['sort'].'+S:E:P+'.$_SESSION['order'].'" class="sortViews"><i class="fa fa-sort"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Trier</span></a></li>
    <li class="divider-vertical hidden-xs"></li>
    <li fa><a href="#" title="Filtrer" data-id="" class="filterViews"><i class="fa fa-filter"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Filtrer</span></a></li>
    '.$liFilter.'
    <li class="divider-vertical hidden-xs"></li>

    <!-- <li '.$classDebug.' fa><a href="index.php?view=DEBUGVIEW"><i class="fa fa-bicycle"></i></a></li>-->' 
    .$adminSection.
    '
    <li fa><a href="#" title="Infos" data-id="" class="apropos"><i class="fa fa-info-circle"></i><span class="hidden-sm hidden-md hidden-lg">&nbsp; Infos</span></a></li>   
    </ul>
    </div><!--/.nav-collapse -->
    </div>
    </nav>

    ';

    return $menu;
}

/**
* Retourne la string du menu de base
* 
*/
function displayLightMenu()
{

    $menu = '

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="'.$_SESSION['urlApp'].'"><img src="'.SP_CORE.DS.SP_IMG.DS.'KT-Start2.png" alt="Logo KT-Start"></a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
    </div><!--/.nav-collapse -->
    </div>
    </nav>

    ';

    return $menu;
}

function KTShowSubDir($directory, $infosTxt)
{

    $dirname = $directory;
    $gitrepo = NULL;

    $dir = opendir($dirname) or die('<p>Erreur de listage : le répertoire n\'existe pas</p>');
    $dossier = array();

    /* Initialisation du tableau $dossiers avec le nom du  dossier comme clé
    et le chemin vers le fichier INFOS.txt en tant que valeur */

    while($element = readdir($dir))
    {
        if($element != '.' && $element != '..')
        {
            if (is_dir($dirname.'/'.$element))
            {

                $dossier[$element] = $dirname.'/'.$element.'/'.$infosTxt;
            }
        }



    }

    closedir($dir);
    asort($dossier);


    /* Affichage du tableau $dossier et mise en tooltip (survol du lien/nom du dossier)
    du contenu du fichier INFOS.TXT  */

    if(!empty($dossier))
    {
        asort($dossier);
        echo '<ul id="listProjets">';
        foreach($dossier as $cle => $lien)
        {
            $fichier = $lien;
            $fileContent = @file_get_contents($fichier);

            echo '<li class="proj"><span class="KT_font13"><a class="xToolTip mini" title="$fileContent" target="_blank" href="'.$dirname.'/'.$cle.'">'.$cle.'</a></span>';
            if(!empty($fileContent)) echo'<div class="KTexplications">'.$fileContent.'</div>';
            if($gitrepo) echo'GIT REPOSITORY';
            echo '</li>';
        }
        echo '</ul>';
    }else
        echo 'LE DOSSIER <strong>'.$directory.'</strong> EST VIDE';


}

/**
* Retourne la string du formulaire d'identification
* 
*/
function displayFormIdentification()
{

    $string='
    <div class="row el_top20">
    <div class="text-center">
    <form id="formLogin" class="form-inline" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">
    <div class="form-group">
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
    </div>
    <input type="hidden" id="action" name="action" value="identification">
    <input type="hidden" id="tokenForm" name="tokenForm" value="'.getToken().'">
    <input type="hidden" id="passwordK" name="passwordK">
    <button type="submit" class="btn btn-default">Sign</button>
    </form>

    </div>
    </div>

    '; 

    return $string;   
}
/**
* Retourne la string du formulaire d'ajout de nouveaux liens (Tools)
* 
*/
function displayFormTools()
{

    $string='
    <!-- .end div row-->
    <div class="row el_top20 el_bot20">
    <div class="text-center"><button type="button" class="btn btn-default el_bot20" data-toggle="collapse" data-target="#tools"><i class="fa fa-chevron-down"></i></button></div>
    <div id="tools" class="collapse">
    <p class="text-center KT_color_2">Ajouter une Url</p>
    <form class="form-inline" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">
    <div class="input-group">
    <select class="form-control selectpicker" id="bgThumb" name="bgThumb">
    <option value="lightBlue" data-content="<span><img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightBlue.png\'></span>">Light Blue</option>
    <option value="deepBlue" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepBlue.png\'>">Deep Blue</option>
    <option value="grassGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-grassGreen.png\'>">Grass Green</option>
    <option value="lightGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightGreen.png\'>">Light Green</option>
    <option value="lightRed" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightRed.png\'>">Light Red</option>
    <option value="turquoise" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-turquoise.png\'>">Turquoise</option>
    <option value="blueSky" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-blueSky.png\'>">Blue Sky</option>
    <option value="camouflage" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-camouflage.png\'>">Camouflage</option>
    <option value="deepPurple" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepPurple.png\'>">Deep Purple</option>
    <option value="lightOrange" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightOrange.png\'>">Light Orange</option>
    <option value="lightViolet" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightViolet.png\'>">Light Violet</option>
    <option value="lightYellow" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightYellow.png\'>">Light Yellow</option>
    </select>
    </div>
    <div class="input-group">
    <input type="text" class="form-control urlBadge" id="urlBadge" name="urlBadge" placeholder="http://votre.url">
    <span class="input-group-btn">
    <input type="hidden" id="action" name="action" value="addBadge">
    <button type="submit" class="btn btn-default">Add</button>
    </span>
    </div>
    </form>
    </div>
    </div>
    <!-- .end div row-->

    '; 

    return $string;   
}

/**
* Génère et retourne un token
* 
*/
function getToken()
{
    return sha1(uniqid(rand()));
}


/**
* Fonction qui retourne certaines données de l'url passée en paramètre
* 
* @param mixed $url
*/
function getUrlData($url)
{
    $result = false;

    $contents = getUrlContents($url);

    if (isset($contents) && is_string($contents))
    {
        $title = null;
        $metaTags = null;

        preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );

        if (isset($match) && is_array($match) && count($match) > 0)
        {
            $title = strip_tags($match[1]);
        }

        preg_match_all('/<[\s]*meta[\s]*name="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 3)
        {
            $originals = $match[0];
            $names = $match[1];
            $values = $match[2];

            if (count($originals) == count($names) && count($names) == count($values))
            {
                $metaTags = array();

                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                    $metaTags[$names[$i]] = array (
                        'html' => htmlentities($originals[$i]),
                        'value' => $values[$i]
                    );
                }
            }
        }

        $result = array (
            'title' => $title,
            'metaTags' => $metaTags
        );
    }

    return $result;
}

function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
    $result = false;

    $contents = @file_get_contents($url);

    // Check if we need to go somewhere else

    if (isset($contents) && is_string($contents))
    {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
        {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
            {
                return getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }

            $result = false;
        }
        else
        {
            $result = $contents;
        }
    }

    return $contents;
}

function getItemIniFile($file, $item_recherche, $groupe_recherche)
{
    $valeur = false;

    if(file_exists($file) && $fichier_lecture=file($file))
    {
        foreach($fichier_lecture as $ligne)
        {
            $ligne_propre = trim($ligne);

            if(preg_match("#^\[(.+)\]$#",$ligne_propre,$matches)){
                $groupe = $matches[1];
            }else{

                if($groupe == $groupe_recherche)
                {
                    if(strpos($ligne,$item_recherche."=")=== 0)
                    {
                        $expl = @explode("=",$ligne,2);    
                        $valeur = end($expl);
                    }elseif($ligne == $item_recherche)
                        $valeur = '';
                }

            }
        }
    }else{
        return 'PARAMETER ERROR';
    }

    if($valeur === FALSE)
        // Groupe ou item inexistant
        return FALSE;
    else
        return $valeur;

}


function displayDebugView($Datas, $key, $order, $Session, $filter)
{
    //DEBUG //

    echo 'SORT: '.$_SESSION['sort'].'<br>';
    echo 'order: '.$_SESSION['filter'].'<br>';
    echo 'TIMESTAMP: '.time().'<br>';
    echo 'VIEW: '.$_SESSION['view'].'<br>';

    if($_SESSION['order'] == 3 )
        $sorder = 'SORT_DESC';
    elseif($_SESSION['order'] == 4 )
        $sorder = 'SORT_ASC';

    echo 'ORDER: '.$sorder.'<br>';

    echo '<pre>';
    //$datas_array = $Datas->sortDatas($Datas->getDatas(), $key, $order, $Session);
    //print_r($datas_array);
    print_r($Datas->sortDatas($Datas->getDatas(), $key, $order, $Session, $filter));
    //echo $Data->sortDatas($Data->getDatas(), $key, $order, $Session); 
    echo '<pre>';
    //die();

}

/**
* Affiche la vue: BADGEVIEW
* 
* @param object $Datas: Objet Datas
* @param string $key: Clé de tri
* @param strinf $order: Ordre de tri
* @param object $Session: Objet Session
*/
function displayBadgesView( $Datas, $key, $order, $Session, $filter, $list = NULL )
{
    $datas_array = $Datas->sortDatas($Datas->getDatas(), $key, $order, $Session, $filter, $list);
    
    echo displayListTabs($Datas);
    
    if(!empty($datas_array))
    {
        foreach ($datas_array as $k => $v) {
            foreach ($v as $k2 => $v2) {

                switch($k2) {
                    case 'visibility': $visibilityRead = $v2;
                        break;
                    case 'list': $listRead = $v2;
                        break;
                    case 'url': $urlRead = $v2;
                        break;                            
                    case 'host': $hostRead = $v2;
                        break;
                    case 'title': $titleRead = $v2;
                        break;
                    case 'description': $descriptionRead = $v2;
                        break;
                    case 'textThumb': $textThumbRead = $v2;
                        break;
                    case 'date': $dateRead = $v2;
                        break;
                    case 'timestamp': $timestampRead = $v2;
                        break;
                    case 'bgThumb': $bgThumbRead = $v2;
                        break;
                    case 'tags': $tagsRead = $v2;
                        break;

                    case 'filename': $filenameRead = $v2;
                        break;
                }

            }
            echo $Datas->createBadge($urlRead, $listRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $timestampRead, $Session);    
        }
    }else {
        echo '<div class="listTabs el_left15 verticalPadding30 el_green KT_font13 text-center">La liste ne contient pas de données...<br /> Ajouter votre première url ou vérifier si il n\'y a pas un filtre actif <br /> Filtre actuel:  <span class="el_red">'.$_SESSION['filter'].'</span></div>';
    }


}

/**
* Affiche la vue: ARRAYVIEW
* 
* @param object $Datas: Objet Datas
* @param string $key: Clé de tri
* @param strinf $order: Ordre de tri
* @param object $Session: Objet Session
*/
function displayArrayView($Datas, $key, $order, $Session, $filter, $list = NULL)
{
    $nbThumb = 1;
    $path = SP_CORE.DS.SP_IMG.DS.'dots'.DS;

    $datas_array = $Datas->sortDatas($Datas->getDatas(), $key, $order, $Session, $filter, $list);
    // Affiche les colonnes de la table en fonction de la session 
    echo displayListTabs($Datas);
   
    if(!empty($datas_array))
    {
        echo createStartTable($Session);

        foreach ($datas_array as $k => $v) {
            foreach ($v as $k2 => $v2) {

                switch($k2) {
                    case 'url': $urlRead = $v2;
                        break;
                    case 'list': $listRead = $v2;
                        break;
                    case 'host': $hostRead = $v2;
                        break;
                    case 'title': $titleRead = $v2;
                        break;
                    case 'description': $descriptionRead = $v2;
                        break;
                    case 'textThumb': $textThumbRead = $v2;
                        break;
                    case 'date': $dateRead = $v2;
                        break;
                    case 'timestamp': $timestampRead = $v2;
                        break;
                    case 'bgThumb': $bgThumbRead = $v2;
                        break;
                    case 'tags': $tagsRead = $v2;
                        break;
                    case 'visibility': $visibilityRead = $v2;
                        break;
                    case 'filename': $filenameRead = $v2;
                        break;
                }

            }

            echo $Datas->createRow($nbThumb, $path, $urlRead, $listRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $timestampRead, $Session);    
            $nbThumb++;
        }

        echo '</table>';
        echo '</div>';
    }else {
       echo '<div class="listTabs el_left15 verticalPadding30 el_green KT_font13 text-center">La liste ne contient pas de données...<br /> Ajouter votre première url ou vérifier si il n\'y a pas un filtre actif <br /> Filtre actuel:  <span class="el_red">'.$_SESSION['filter'].'</span></div>';
    }

}

/**
* Affiche la vue: LISTVIEW
* 
* @param object $Datas: Objet Datas
* @param string $key: Clé de tri
* @param strinf $order: Ordre de tri
* @param object $Session: Objet Session
*/
function displayListView($Datas, $key, $order, $Session, $filter, $list = NULL)
{
    $nbThumb = 1;
    $path = SP_CORE.DS.SP_IMG.DS.'dots'.DS;
    $datas_array = $Datas->sortDatas($Datas->getDatas(), $key, $order, $Session, $filter, $list);

    echo displayListTabs($Datas);
    
    if(!empty($datas_array))
    { 
        echo '<div  class="col el_top20">';

        foreach ($datas_array as $k => $v) {
            foreach ($v as $k2 => $v2) {

                switch($k2) {
                    case 'url': $urlRead = $v2;
                        break;
                    case 'list': $listRead = $v2;
                        break;
                    case 'host': $hostRead = $v2;
                        break;
                    case 'title': $titleRead = $v2;
                        break;
                    case 'description': $descriptionRead = $v2;
                        break;
                    case 'textThumb': $textThumbRead = $v2;
                        break;
                    case 'date': $dateRead = $v2;
                        break;
                    case 'timestamp': $timestampRead = $v2;
                        break;
                    case 'bgThumb': $bgThumbRead = $v2;
                        break;
                    case 'tags': $tagsRead = $v2;
                        break;
                    case 'visibility': $visibilityRead = $v2;
                        break;
                    case 'filename': $filenameRead = $v2;
                        break;
                }

            }

            echo $Datas->createList($nbThumb, $path, $urlRead, $listRead, $hostRead, $titleRead, $descriptionRead, $bgThumbRead, $textThumbRead, $dateRead, $tagsRead, $visibilityRead, $filenameRead, $timestampRead, $Session);    
            $nbThumb++;
        }

        echo '</div>';
    }else {
        echo '<div class="listTabs el_left15 verticalPadding30 el_green KT_font13 text-center">La liste ne contient pas de données...<br /> Ajouter votre première url ou vérifier si il n\'y a pas un filtre actif <br /> Filtre actuel:  <span class="el_red">'.$_SESSION['filter'].'</span></div>';
    }

}

/**
* Fonction qui affiche les informations  
* sous la forme d'une Ligne d'un tableau
*  

* @return string : Retourne Le début de la table
*/
function createStartTable($Session) {


    $theadConsult = '
    <div class="table-responsive el_top20">
    <table class="table table-striped table-hover table-condensed table-primary">
    <tr class="thTitle">
    <th class="text-center">N°</th>
    <th class="text-center hidden-xs">Badge</th>
    <th class="text-center">Site</th>
    <th class="text-center hidden-xs">Titre</th>
    <th class="text-center hidden-xs">Description</th>
    <th class="text-center hidden-xs">Etiquette</th>
    </tr>';

    $theadAdmin = '
    <div class="table-responsive el_top20">
    <table class="table table-striped table-hover table-condensed table-primary">
    <tr class="thTitle">
    <th class="text-center">N°</th>
    <th class="text-center hidden-xs">Badge</th>
    <th class="text-center">Site</th>
    <th class="text-center hidden-xs">Titre</th>
    <th class="text-center hidden-xs">Description</th>
    <th class="text-center hidden-xs">Tags</th>
    <th class="text-center"><i class="fa fa-trash-o"></i></th>
    <th class="text-center"><i class="fa fa-pencil fa-lg"></i></th>
    </tr>';


    if($Session->sessionOpen())
        return $theadAdmin;
    else
        return $theadConsult;


}// End function createStartTable()

/**
* Fonction qui compte le nombre de fichiers présents dans le répertoire 
* passé en paramètre
* 
* @param mixed $dir
*/
function countFileDir($dir)
{
    $nbFiles = 0;

    if( $dossier = opendir( $dir ) )
    {
        while( false != ($fichier = readdir( $dossier )) )
        {

            if( $fichier != '.' && $fichier != '..')
                $nbFiles ++;
        }     
        closedir( $dossier );
    }
    return $nbFiles;    
}


/**
* Vérifie l'existence d'une URL
* @param string $url
* @return boolean
*/
function url_exist($url){

    if(empty($url))
        return false;

    $context = stream_context_create(array('http'=>array('timeout'=>3)));
    $handle = @fopen($url, 'r', false, $context);  

    if($handle)
    {
        fclose($handle);
        return true;
    }
    else
        return false;

}

/**
* Cherche un Clé dans une url parsée
* 
* @param array $url
* @param string $key
* @return boolean
*/
function key_finder_urlparsed($url, $key) {

    if(is_array($url)) {
        foreach($url as $k => $v) {
            if($k == $key)
                return TRUE;
            else
                return FALSE;
        }
    }else
        return FALSE;
}

/**
* Cette fonction retourne le domaine principal d'une URL
* 
* @param string $url :  Url à parser 
*/
function getPrimaryDomain($url) 
{
    $tld = parse_url($url,PHP_URL_HOST);
    $tldArray = explode(".",$tld);

    // COUNTS THE POSITION IN THE ARRAY TO IDENTIFY THE TOP LEVEL DOMAIN (TLD)
    $l1 = '0';

    foreach($tldArray as $s) {
        // CHECKS THE POSITION IN THE ARRAY TO SEE IF IT MATCHES ANY OF THE KNOWN TOP LEVEL DOMAINS (YOU CAN ADD TO THIS LIST)
        if($s == 'com' || $s == 'net' || $s == 'info' || $s == 'biz' || $s == 'us' || $s == 'co' || $s == 'org' 
        || $s == 'me' || $s == 'fr' || $s == 'be' || $s == 'de' || $s == 'jp' || $s == 'ovh' || $s == 'io' || $s == 'eu'
        || $s == 'in' || $s == 'lu'
        ) {

            // CALCULATES THE SECOND LEVEL DOMAIN POSITION IN THE ARRAY ONCE THE POSITION OF THE TOP LEVEL DOMAIN IS IDENTIFIED
            $l2 = $l1 - 1;   
        }
        else {
            // INCREMENTS THE COUNTER FOR THE TOP LEVEL DOMAIN POSITION IF NO MATCH IS FOUND
            $l1++;
        }
    }

    // RETURN THE SECOND LEVEL DOMAIN AND THE TOP LEVEL DOMAIN IN THE FORMAT LIKE "SOMEDOMAIN.COM"
    if(isset($l2))
        return $tldArray[$l2] . '.' . $tldArray[$l1];
    else
        return FALSE;
}

function checkIP($ip)
{
    if ( preg_match('#^([0-9]{1,3}\.){3}[0-9]{1,3}$#', $ip ) )
    {
        return TRUE;
    }else
        return FALSE;
}

function frenchChar($text)
{
    $text = htmlentities($text, ENT_NOQUOTES, "UTF-8");
    $text = htmlspecialchars_decode($text);
    return $text;
}

/**
* Test l'existence d'un fichier
* 
* @param string $file
* @return boolean 
*/
function checkFile($file) {

    if(file_exists($file))
        return TRUE;
    else
        return FALSE;

}

/**
* Retourne la string de la fenêtre modale de suppression
* 
*/
function displayModalDelete()
{

    $string='
    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Supprimer le badge" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-trash-o"></i> &nbsp;Suppression du badge</h4>
    </div>

    <form class="form-inline" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">
    <div class="modal-body">
    <p class="text-center ">Etes-vous certain de vouloir supprimer ce badge ?</p>
    <p class="text-center"><i class="fa fa-globe ico-link"></i> &nbsp;<a id="lien" class="ico-link"></a></p>
    <input type="hidden" id="id" name="id">
    <input type="hidden" id="action" name="action" value="delete">
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
    <button type="submit" class="btn btn-danger">Supprimer</button>
    </div>
    </form>

    </div>
    </div>
    </div>
    ';

    return $string;

}

/**
*  Retourne la string de la fenêtre modale d'édition
* 
* @param object $Datas
*/
function displayModalEdit($Datas)
{

    // Création de la liste déroulante des listes de badges
    $listing = displayListSelect($Datas);

    $string='
    <div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Editer le badge" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-pencil"></i> &nbsp;Edition du badge</h4>
    </div>

    <form class="form-horizontal" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">

    <div class="modal-body">
    <p class="text-center"><i class="fa fa-globe ico-link"></i> &nbsp;<a id="lien" class="ico-link"></a></p>
    <div class="form-group">
    <label for="url" class="col-sm-2 control-label KTlabel">Url</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="url" name="url">
    </div>
    </div>
    <div class="form-group">
    <label for="ktlist" class="col-sm-2 control-label KTlabel">Liste</label>
    <div class="col-sm-10">
    '.$listing.'
    </div>
    </div>
    <div class="text-center"><button type="button" class="btnCollapseTabs btn-default el_bot20" data-toggle="collapse" data-target="#divnewlist"><i class="fa fa-chevron-down"></i> | Créer une nouvelle liste</button></div>
    <div id="divnewlist" class="form-group">
    <label for="newlist" class="col-sm-2 control-label KTlabel">Créer Liste</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="newlist" name="newlist">
    </div>
    </div>
    <div class="form-group">
    <label for="url" class="col-sm-2 control-label KTlabel">Host</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="host" name="host">
    </div>
    </div>
    <div class="form-group">
    <label for="titre" class="col-sm-2 control-label KTlabel">Titre</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="titre" name="titre">
    </div>
    </div>
    <div class="form-group">
    <label for="description" class="col-sm-2 control-label KTlabel">Description</label>
    <div class="col-sm-10">
    <textarea type="text" class="form-control" id="description" name="description"></textarea>
    </div>
    </div>
    <div class="form-group">
    <label for="bgThumb" class="col-sm-2 control-label KTlabel">Badge</label>
    <div class="col-sm-10">
    <select class="form-control selectpicker" id="bgThumbEdit" name="bgThumbEdit">
    <option value="lightBlue" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightBlue.png\'>">Light Blue</option>
    <option value="deepBlue" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepBlue.png\'>">Deep Blue</option>
    <option value="grassGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-grassGreen.png\'>">Grass Green</option>
    <option value="lightGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightGreen.png\'>">Light Green</option>
    <option value="lightRed" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightRed.png\'>">Light Red</option>
    <option value="turquoise" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-turquoise.png\'>">Turquoise</option>
    <option value="blueSky" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-blueSky.png\'>">Blue Sky</option>
    <option value="camouflage" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-camouflage.png\'>">Camouflage</option>
    <option value="deepPurple" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepPurple.png\'>">Deep Purple</option>
    <option value="lightOrange" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightOrange.png\'>">Light Orange</option>
    <option value="lightViolet" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightViolet.png\'>">Light Violet</option>
    <option value="lightYellow" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightYellow.png\'>">Light Yellow</option>
    </select>
    <label id="textBgThumb" class="txtBgThumb"></label>
    </div>
    </div>
    <div class="form-group">
    <label for="textThumb" class="col-sm-2 control-label KTlabel">Texte du Badge</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="textThumb" name="textThumb">
    </div>
    </div>
    <div class="form-group">
    <label for="tags" class="col-sm-2 control-label KTlabel">Tags</label>
    <div class="col-sm-10">
    <input type="text" class="form-control" id="tags" name="tags">
    </div>
    </div>
    <div class="form-group">
    <label for="visibility" class="col-sm-2 control-label KTlabel">Visibilité</label>
    <div class="col-sm-10">
    <select class="form-control" id="visibility" name="visibility">
    <option value="public">Publique</option>
    <option value="private">Privé</option>
    </select>
    </div>
    </div>
    <input type="hidden" id="id" name="id">
    <input type="hidden" id="timestamp" name="timestamp">
    <input type="hidden" id="date" name="date">
    <input type="hidden" id="action" name="action" value="edition">
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
    <button type="submit" class="btn btn-primary">Modifier</button>
    </div>
    </form>

    </div>
    </div>
    </div>
    ';

    return $string;

}

/**
* ConstRetourne la string de la fenêtre modale de tri
* 
*/
function displayModalSort()
{


    $string='
    <div id="sort-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Trier vos liens" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-sort"></i> &nbsp;Tri des Vues</h4>
    </div>

    <form class="form-horizontal" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">
    <div class="modal-body">
    <div class="form-group">
    <label for="bgThumb" class="col-sm-2 control-label KTlabel">Champs</label>
    <div class="col-sm-10">
    <select class="form-control selectpicker" id="field" name="field">
    <option value="tags">Tag du Badge</option>
    <option value="textThumb">Texte du Badge</option>
    <option value="bgThumb">Couleur du Badge</option>
    <option value="timestamp">Date de création</option>
    <option value="url">Url</option>
    <option value="host">Host</option>
    <option value="title">Titre</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="direction" class="col-sm-2 control-label KTlabel">Tri</label>
    <div class="col-sm-10">
    <label class="radio-inline direction">
    <input type="radio" name="direction" id="direction_asc" value="SORT_ASC">ASC</label>
    <label class="radio-inline">
    <input type="radio" name="direction" id="direction_desc" value="SORT_DESC">DESC</label>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <input type="hidden" id="action" name="action" value="tri">
    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
    <button type="submit" class="btn btn-primary">Modifier</button>
    </div>
    </form>

    </div>
    </div>
    </div>
    ';

    return $string;

}

function generateTagsCloud($Datas) {

    $tags_designed = NULL;

    $tags_array = $Datas->getTagsArray();

    if (is_array($tags_array)) {
        foreach($tags_array as $k => $v) {
            $tags_designed .= '<a href="index.php?view='.$_SESSION['view'].'&filter='.$v.'"><span class="btn KTbtn btnGreen KTFontLight">'.$v.'</span></a>&nbsp;';
        }
        $tags_designed .= '<a href="index.php?view='.$_SESSION['view'].'&filter=aucun"><span class="btn KTbtn btnRed KTFontLight">réinitialiser</span></a>&nbsp;';
        return $tags_designed;
    }else
        return '<span class="el_red">Function generateTagsCloud - Error</span>';


}

/**
* Retourne la string de la fenêtre modale de filtrage
* 
*/
function displayModalFilter($Datas)
{


    $string='
    <div id="filter-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Filtrage des vues" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-filter"></i> &nbsp;Filtrage des Vues</h4>
    </div>
    <div class="modal-body">
    <p>Sélectionner une étiquette pour filtrer l\'affichage</p>';

    $string .= generateTagsCloud($Datas);

    $string .=
    ' 
    <!-- End .modal-body -->
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    <!-- End .modal-footer -->
    </div>
    <!-- End .modal-content -->
    </div>
    <!-- End .modal-dialog -->
    </div>
    <!-- End .modal fade -->
    </div>
    ';

    return $string;

}

/**
* Retourne la string de la fenêtre modale d'informations
* 
*/
function displayModalInfos($Session, $Datas)
{

    $string='
    <div id="infos-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="A propos de KT Start" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-info-circle"></i> &nbsp;A propos de KT Start</h4>
    </div>                      
    <div class="modal-body">

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Numéro de version</label></div>
    <div class="col-md-8">'.SP_VER.'</div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Date de mise à jour</label></div>
    <div class="col-md-8">'.SP_DATEMAJ.'</div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Site web de KT Start</label></div>
    <div class="col-md-8"><a href="http://www.ktstart.org" target="_blank">www.ktstart.org</a></div>
    </div><!-- End .row -->
    
    <div class="row">
    <div class="col-md-4 KTlabel"><label>Sources de KT Start</label></div>
    <div class="col-md-8"><a href="http://sources.xoransorvor.be:3000/KTDev/KT-Start" target="_blank">Code source de KT Start</a></div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Auteur</label></div>
    <div class="col-md-8">KTDev - <a href="http://ktdev.info/contact.php">Kentaro Tatsu</a></div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Site web de l\'auteur</label></div>
    <div class="col-md-8"><a href="http://www.ktdev.info" target="_blank">www.ktdev.info</a></div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Licence</label></div>
    <div class="col-md-8">Soumis à la licence <a href="https://opensource.org/licenses/mit-license.php" target="_blank">MIT</a></div>
    </div><!-- End .row -->

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Description</label></div>
    <div class="col-md-8 el_grey text-justify">
    <p>KT Start est une application web qui gère vos favoris en ligne via votre navigateur.<p>
    <p>Conservez, classez et gérez vos liens favoris indépendamment de votre système d\'exploitation et de votre navigateur en toute simplicité.</p>
    <p>KT Start est développé en php et ne nécessite aucune base de données pour fonctionner.</p>
    <p> Pour l\'installer, il suffit de <a href="http://sources.xoransorvor.be:3000/KTDev/KT-Start/releases">télécharger</a> la dernière release, de désarchiver le fichier sur votre ordnateur 
    et d\'utiliser un client ftp pour transférer les fichiers sur votre serveur dans l\'emplacement de votre choix. Pour terminer, rendez-vous sur l\'url pour complèter le mini formulaire de pré-configuration.</p>

    </div>
    </div><!-- End .row -->

    </div><!-- End .modal-body -->
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    </div><!-- End .modal-footer -->
    </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
    </div><!-- End .modal fade -->
    ';

    return $string;

}

/**
* Retourne la string de la fenêtre modale d'outils
* 
*/
function displayModalTools()
{

    if(PHP_OS == 'Linux') {
        $FlagOS = '<i class="fa fa-check-square flagSquare" title="Fonctionnalité disponible"></i>';
        $status = '';
    }else {
        $FlagOS = '<i class="fa fa-minus-circle flagMinus" title="Fonctionnalité non disponible"></i>';
        $status = 'disabled';
    }

    $string='
    <div id="tools-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="KT Start - Outils" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-wrench"></i> &nbsp;KT Start - Outils</h4>
    </div>                      
    <div class="modal-body">

    <div class="row">
    <div class="col-md-4 KTlabel"><label>Infos Serveur</label></div>
    <div class="col-md-8"><a href="server-infos.php"><button type="button" class="btn btn-default">Phpinfo() du serveur</button></a></div>
    </div><!-- End .row -->

    <hr class="el_top20">
    <div class="row el_top20">
    <div class="col-md-4 KTlabel"><label>Export des données</label></div>
    <div class="col-md-8">
    <form action="index.php?view='.$_SESSION['view'].'" method="post">
    <input type="hidden" id="action" name="action" value="export">
    <input type="submit" class="btn btn-default" value="Exporter vos données" '.$status.'>
    <p class="el_top10"><small>GNU/Linux server Only &nbsp;'.$FlagOS.'</small></p>
    </form>
    </div>                                                                   
    </div><!-- End .row -->

    <hr class="el_top20">
    <div class="row el_top20">
    <div class="col-md-4 KTlabel"><label>Import des données</label></div>
    <div class="col-md-8">
    <form action="index.php?view='.$_SESSION['view'].'" method="post" enctype="multipart/form-data">
    <input id="importFile" name="importFile" type="file" class="btn btn-default" '.$status.'>
    <p class="el_top10"><small>GNU/Linux server Only &nbsp;'.$FlagOS.'</small></p>
    <input type="hidden" id="action" name="action" value="import">
    <input type="submit" class="btn btn-default el_top10" value="Importer" '.$status.'>
    </form>
    </div>                                                                   
    </div><!-- End .row -->

    </div><!-- End .modal-body -->
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
    </div><!-- End .modal-footer -->
    </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
    </div><!-- End .modal fade -->
    ';

    return $string;

}

/**
* Retourne la string de la fenêtre modale d'édition
* 
*/
function displayModalParams ()
{


    $string='
    <div id="params-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Paramètrer KT Start" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-center" id="myModalLabel"><i class="fa fa-cog"></i> &nbsp;Paramètres de KT Start</h4>
    </div>

    <form class="form-horizontal" action="index.php?view='.$_SESSION['view'].'" role="form" method="POST">

    <div class="modal-body">

    <div class="form-group">
    <label for="url" class="col-sm-4 control-label KTlabel">Url de l\'application</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="urlAppParam" name="urlAppParam">
    </div>
    </div>
    <div class="form-group">
    <label for="titre" class="col-sm-4 control-label KTlabel">Vue par défaut</label>
    <div class="col-sm-8">
    <select class="form-control" id="defaultViewParam" name="defaultViewParam">
    <option value="BADGESVIEW">Badge</option>
    <option value="ARRAYVIEW">Tableau</option>
    <option value="LISTVIEW">Liste</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="description" class="col-sm-4 control-label KTlabel">Attribut target des liens</label>
    <div class="col-sm-8">
    <select class="form-control" id="targetParam" name="targetParam">
    <option value="_blank">Ouvrir dans un nouvel onglet (_blank)</option>
    <option value="_self">Ouvrir dans le même onglet (_self)</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="bgThumb" class="col-sm-4 control-label KTlabel">Badge par défaut</label>
    <div class="col-sm-8">
    <select class="form-control selectpicker" id="bgThumbParam" name="bgThumbParam">
    <option value="lightBlue" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightBlue.png\'>">Light Blue</option>
    <option value="deepBlue" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepBlue.png\'>">Deep Blue</option>
    <option value="grassGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-grassGreen.png\'>">Grass Green</option>
    <option value="lightGreen" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightGreen.png\'>">Light Green</option>
    <option value="lightRed" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightRed.png\'>">Light Red</option>
    <option value="turquoise" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-turquoise.png\'>">Turquoise</option>
    <option value="blueSky" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-blueSky.png\'>">Blue Sky</option>
    <option value="camouflage" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-camouflage.png\'>">Camouflage</option>
    <option value="deepPurple" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-deepPurple.png\'>">Deep Purple</option>
    <option value="lightOrange" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightOrange.png\'>">Light Orange</option>
    <option value="lightViolet" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightViolet.png\'>">Light Violet</option>
    <option value="lightYellow" data-content="<img src=\''.SP_CORE.DS.SP_IMG.DS.'dots'.DS.'dot-lightYellow.png\'>">Light Yellow</option>
    </select>
    <label id="textBgThumb" class="txtBgThumb"></label>
    </div>
    </div>
    <div class="form-group">
    <label for="bgThumb" class="col-sm-4 control-label KTlabel">Tri par défaut</label>
    <div class="col-sm-8">
    <select class="form-control selectpicker" id="sortParam" name="sortParam">
    <option value="tags">Tag du Badge</option>
    <option value="textThumb">Texte du Badge</option>
    <option value="bgThumb">Couleur du Badge</option>
    <option value="timestamp">Date de création</option>
    <option value="url">Url</option>
    <option value="host">Host</option>
    <option value="title">Titre</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="textThumb" class="col-sm-4 control-label KTlabel">Ordre de tri par défaut</label>
    <div class="col-sm-8">
    <select class="form-control" id="orderParam" name="orderParam">
    <option value="SORT_ASC">Tri ascendant</option>
    <option value="SORT_DESC">Tri descendant</option>
    </select>
    </div>
    </div>
    <div class="form-group">
    <label for="tags" class="col-sm-4 control-label KTlabel">Etiquette par défaut</label>
    <div class="col-sm-8">
    <input type="text" class="form-control" id="tagsParam" name="tagsParam">
    </div>
    </div>
    <div class="form-group">
    <label for="visibility" class="col-sm-4 control-label KTlabel">Visibilité par défaut</label>
    <div class="col-sm-8">
    <select class="form-control" id="visibilityParam" name="visibilityParam">
    <option value="public">Publique</option>
    <option value="private">Privé</option>
    </select>
    </div>
    </div>
    <input type="hidden" id="action" name="action" value="editParams">
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
    <button type="submit" class="btn btn-primary">Modifier</button>
    </div>
    </form>

    </div>
    </div>
    </div>
    ';

    return $string;

}

/**
*  Retourne la string d'affichage HTML des listes des badges sous la forme d'une liste déroulante
* 
* @param object $Datas
*/
function displayListSelect($Datas, $class = NULL)
{

    // Initialiser le tableau des listes
    $ktlist = $Datas->getListsArray();
    $string = NULL;


    $string .= '<select class="form-control '.$class.'" id="ktlist" name="ktlist">';

    // Créer le contenu du select
    if(!empty($ktlist)) {
        foreach($ktlist as $key => $val)    
        {
            $string .= '<option value="'.$val.'">'.$val.'</option>';    
        }   
    }
    
    $string .= '</select>';  

    return $string;

}

/**
*  Retourne la string d'affichage HTML des listes des badges sous la forme d'étiquettes/onglet
* 
* @param object $Datas
*/
function displayListTabs($Datas)
{

    // Initialiser le tableau des listes
    $ktlist = $Datas->getListsArray();
    $string = NULL;
    $selected = NULL;


    if (is_array($ktlist)) {
        
        $string .= '<div class="listTabs KT_left15 text-center">';
        
        if(strtoupper($_SESSION['list']) == 'ALL')
            $string .= '<span class="btn KTFontLight btnListSelected"><a href="index.php?view='.$_SESSION['view'].'&filter='.$_SESSION['filter'].'&list=ALL" class="aList tabSelected">ALL</a></span>&nbsp;';
        else
            $string .= '<span class="btn btnListTab KTFontLight"><a href="index.php?view='.$_SESSION['view'].'&filter='.$_SESSION['filter'].'&list=ALL" class="aList">ALL</a></span>&nbsp;';

        foreach($ktlist as $k => $v) {
            if(strtoupper($_SESSION['list']) == strtoupper($v))
                $string .= '<span class="btn KTFontLight btnListSelected"><a href="index.php?view='.$_SESSION['view'].'&filter='.$_SESSION['filter'].'&list='.$v.'" class="aList tabSelected">'.$v.'</a></span>&nbsp;';
            else
                $string .= '<span class="btn btnListTab KTFontLight"><a href="index.php?view='.$_SESSION['view'].'&filter='.$_SESSION['filter'].'&list='.$v.'" class="aList">'.$v.'</a></span>&nbsp;';
        }

        $string .= '</div>';
        
        return $string;
    }/*else
        return '<div class="el_red">KT Start a rencontré un prblème lors de l\'affichage des listes</div>';*/



}

?>
